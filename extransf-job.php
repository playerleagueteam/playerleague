<?php

define("BASE_FOLDER", __DIR__ ."/");

$jobId = "extransf";

define("JOBS_CONFIG_FILE", BASE_FOLDER . "/admin/config/jobs.xml");

include(BASE_FOLDER . "/admin/config/global.inc.php");

$xml = simplexml_load_file(JOBS_CONFIG_FILE);

$jobConfig = $xml->xpath("//job[@id = '". $jobId . "']");

$jobClass = (string) $jobConfig[0]->attributes()->class;

$i18n = I18n::getInstance($website->getConfig("supported_languages"));

$job = new $jobClass($website, $db, $i18n, $jobId);

$job->execute();
?>

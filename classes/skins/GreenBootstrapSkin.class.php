<?php
/******************************************************

  This file is part of Player League.

  Player League is free software: you can redistribute it 
  and/or modify it under the terms of the 
  GNU Lesser General Public License 
  as published by the Free Software Foundation, either version 3 of
  the License, or any later version.

  Player League is distributed in the hope that it will be
  useful, but WITHOUT ANY WARRANTY; without even the implied
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public 
  License along with Player League.  
  If not, see <http://www.gnu.org/licenses/>.

******************************************************/

/**
 * Same as DefaultBootstrapSkin, but with green colors.
 * 
 * @author Ingo Hofmann
 */
class GreenBootstrapSkin extends DefaultBootstrapSkin {
	
	/**
	 * @see DefaultBootstrapSkin::getCssSources()
	 */
	public function getCssSources() {
		$dir = $this->_websoccer->getConfig('context_root') . '/css/';
		$files[] = $dir . 'boot3/bootstrap.css';
		$files[] = $dir . 'defaultskin.css';
		$files[] = $dir . 'websoccer.css';
		
		$files[] = '//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css';
	
		if ($this->_websoccer->getPageId() == 'formation'
				|| $this->_websoccer->getPageId() == 'training') {
			$files[] = $dir . 'slider.css';
		}
	
		if ($this->_websoccer->getPageId() == 'formation'
				|| $this->_websoccer->getPageId() == 'youth-formation'
				|| $this->_websoccer->getPageId() == 'teamoftheday') {
			$files[] = $dir . 'formation.css';
		}
		
		return $files;
	}
	
		/**
	 * @see ISkin::getJavaScriptSources()
	 */
	public function getJavaScriptSources() {
		$dir = $this->_websoccer->getConfig('context_root') . '/js/';
		$files[] = '//code.jquery.com/jquery-1.11.1.min.js';

		if (DEBUG) {
			$files[] = $dir . '/boot3/bootstrap.js';
			$files[] = $dir . 'jquery.blockUI.js';
			$files[] = $dir . 'wsbase.js';
		} else {
			$files[] = $dir . 'websoccer.min.js';
		}

		return $files;
	}
	
}
?>
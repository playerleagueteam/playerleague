<?php

/* macros/formelements.twig */
class __TwigTemplate_eab66396c5478378b63556a8b53129a3ae81fd5501381fec15c92df82c829cc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 23
        echo "
";
        // line 46
        echo "
";
        // line 59
        echo "
";
        // line 80
        echo "
";
        // line 92
        echo "
";
        // line 102
        echo "
";
        // line 123
        echo "
";
        // line 131
        echo "
";
    }

    // line 1
    public function gettextfield($_fieldId = null, $_label = null, $_value = null, $_required = false, $_errormsg = null, $_type = "text", $_helptext = null, $_disabled = false)
    {
        $context = $this->env->mergeGlobals(array(
            "fieldId" => $_fieldId,
            "label" => $_label,
            "value" => $_value,
            "required" => $_required,
            "errormsg" => $_errormsg,
            "type" => $_type,
            "helptext" => $_helptext,
            "disabled" => $_disabled,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
";
            // line 3
            if ($this->getAttribute((isset($context["errormsg"]) ? $context["errormsg"] : null), (isset($context["fieldId"]) ? $context["fieldId"] : null), array(), "array", true, true)) {
                // line 4
                echo "\t";
                $context["error"] = $this->getAttribute((isset($context["errormsg"]) ? $context["errormsg"] : null), (isset($context["fieldId"]) ? $context["fieldId"] : null), array(), "array");
            } else {
                // line 6
                echo "\t";
                $context["error"] = "";
            }
            // line 8
            echo "<div class=\"form-group";
            if ((twig_length_filter($this->env, (isset($context["error"]) ? $context["error"] : null)) > 0)) {
                echo " has-error";
            }
            echo "\">
\t<label class=\"control-label col-sm-3\" for=\"";
            // line 9
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\">
\t";
            // line 10
            if ((isset($context["required"]) ? $context["required"] : null)) {
                // line 11
                echo "\t\t<strong>";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
                echo "</strong>
\t";
            } else {
                // line 13
                echo "\t\t";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
                echo "
\t";
            }
            // line 15
            echo "\t</label>
\t<div class=\"col-sm-9\">
\t\t<input type=\"";
            // line 17
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo "\" class=\"form-control";
            if (((isset($context["type"]) ? $context["type"] : null) == "number")) {
                echo " input-sm";
            }
            echo "\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "\" placeholder=\"";
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
            echo "\"";
            if ((isset($context["required"]) ? $context["required"] : null)) {
                echo " required";
            }
            if ((isset($context["disabled"]) ? $context["disabled"] : null)) {
                echo " disabled";
            }
            echo " ";
            if ((twig_length_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null)) > 0)) {
                echo "aria-describedby=\"";
                echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
                echo "_helptext\"";
            }
            echo ">
\t\t";
            // line 18
            if ((twig_length_filter($this->env, (isset($context["error"]) ? $context["error"] : null)) > 0)) {
                echo "<span class=\"help-block\">";
                echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
                echo "</span>";
            }
            // line 19
            echo "\t\t";
            if ((twig_length_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null)) > 0)) {
                echo "<span id=\"";
                echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
                echo "_helptext\" class=\"help-block\">";
                echo twig_escape_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null), "html", null, true);
                echo "</span>";
            }
            // line 20
            echo "\t</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 24
    public function getautocomplete($_fieldId = null, $_label = null, $_value = null, $_required = false, $_errormsg = null, $_ajaxblock = null, $_helptext = null)
    {
        $context = $this->env->mergeGlobals(array(
            "fieldId" => $_fieldId,
            "label" => $_label,
            "value" => $_value,
            "required" => $_required,
            "errormsg" => $_errormsg,
            "ajaxblock" => $_ajaxblock,
            "helptext" => $_helptext,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 25
            echo "
";
            // line 26
            if ($this->getAttribute((isset($context["errormsg"]) ? $context["errormsg"] : null), (isset($context["fieldId"]) ? $context["fieldId"] : null), array(), "array", true, true)) {
                // line 27
                echo "\t";
                $context["error"] = $this->getAttribute((isset($context["errormsg"]) ? $context["errormsg"] : null), (isset($context["fieldId"]) ? $context["fieldId"] : null), array(), "array");
            } else {
                // line 29
                echo "\t";
                $context["error"] = "";
            }
            // line 31
            echo "<div class=\"form-group";
            if ((twig_length_filter($this->env, (isset($context["error"]) ? $context["error"] : null)) > 0)) {
                echo " has-error";
            }
            echo "\">
\t<label class=\"control-label col-sm-2\" for=\"";
            // line 32
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\">
\t";
            // line 33
            if ((isset($context["required"]) ? $context["required"] : null)) {
                // line 34
                echo "\t\t<strong>";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
                echo "</strong>
\t";
            } else {
                // line 36
                echo "\t\t";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
                echo "
\t";
            }
            // line 38
            echo "\t</label>
\t<div class=\"col-sm-10\">
\t\t<input type=\"text\" id=\"";
            // line 40
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" class=\"form-control autocomplete\" autocomplete=\"off\" data-provide=\"typeahead\" data-ajaxblock=\"";
            echo twig_escape_filter($this->env, (isset($context["ajaxblock"]) ? $context["ajaxblock"] : null), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "\" placeholder=\"";
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
            echo "\"";
            if ((isset($context["required"]) ? $context["required"] : null)) {
                echo " required";
            }
            echo ">
\t\t";
            // line 41
            if ((twig_length_filter($this->env, (isset($context["error"]) ? $context["error"] : null)) > 0)) {
                echo "<span class=\"help-inline\">";
                echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
                echo "</span>";
            }
            // line 42
            echo "\t\t";
            if ((twig_length_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null)) > 0)) {
                echo "<span class=\"help-inline\">";
                echo twig_escape_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null), "html", null, true);
                echo "</span>";
            }
            // line 43
            echo "\t</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 47
    public function getcheckbox($_fieldId = null, $_label = null, $_value = null, $_colspan = false, $_errormsg = null)
    {
        $context = $this->env->mergeGlobals(array(
            "fieldId" => $_fieldId,
            "label" => $_label,
            "value" => $_value,
            "colspan" => $_colspan,
            "errormsg" => $_errormsg,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 48
            echo "<div class=\"form-group";
            if ((twig_length_filter($this->env, (isset($context["errormsg"]) ? $context["errormsg"] : null)) > 0)) {
                echo " has-error";
            }
            echo "\"\">
\t<div class=\"";
            // line 49
            if (((isset($context["colspan"]) ? $context["colspan"] : null) == false)) {
                echo "col-sm-offset-3 col-sm-9";
            } else {
                echo "col-sm-12";
            }
            echo "\">
\t\t<div class=\"checkbox\">
\t\t    <label>
\t\t      <input type=\"checkbox\" id=\"";
            // line 52
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" value=\"1\"";
            if (((isset($context["value"]) ? $context["value"] : null) == 1)) {
                echo " checked";
            }
            echo "> ";
            echo (isset($context["label"]) ? $context["label"] : null);
            echo "
\t\t    </label>\t
\t    </div>
\t
\t</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 60
    public function gettextarea($_fieldId = null, $_label = null, $_value = null, $_required = false, $_errormsg = null, $_helptext = null, $_rows = 10)
    {
        $context = $this->env->mergeGlobals(array(
            "fieldId" => $_fieldId,
            "label" => $_label,
            "value" => $_value,
            "required" => $_required,
            "errormsg" => $_errormsg,
            "helptext" => $_helptext,
            "rows" => $_rows,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 61
            echo "
";
            // line 62
            if ($this->getAttribute((isset($context["errormsg"]) ? $context["errormsg"] : null), (isset($context["fieldId"]) ? $context["fieldId"] : null), array(), "array", true, true)) {
                // line 63
                echo "\t";
                $context["error"] = $this->getAttribute((isset($context["errormsg"]) ? $context["errormsg"] : null), (isset($context["fieldId"]) ? $context["fieldId"] : null), array(), "array");
            } else {
                // line 65
                echo "\t";
                $context["error"] = "";
            }
            // line 67
            echo "<div class=\"form-group";
            if ((twig_length_filter($this->env, (isset($context["error"]) ? $context["error"] : null)) > 0)) {
                echo " has-error";
            }
            echo "\">
\t<label for=\"";
            // line 68
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\">
\t";
            // line 69
            if ((isset($context["required"]) ? $context["required"] : null)) {
                // line 70
                echo "\t\t<strong>";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
                echo "</strong>
\t";
            } else {
                // line 72
                echo "\t\t";
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
                echo "
\t";
            }
            // line 74
            echo "\t</label>
\t<textarea id=\"";
            // line 75
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" rows=\"";
            echo twig_escape_filter($this->env, (isset($context["rows"]) ? $context["rows"] : null), "html", null, true);
            echo "\" class=\"span5\">";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "</textarea>
\t";
            // line 76
            if ((twig_length_filter($this->env, (isset($context["error"]) ? $context["error"] : null)) > 0)) {
                echo "<span class=\"help-inline\">";
                echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
                echo "</span>";
            }
            // line 77
            echo "\t";
            if ((twig_length_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null)) > 0)) {
                echo "<span class=\"help-inline\">";
                echo twig_escape_filter($this->env, (isset($context["helptext"]) ? $context["helptext"] : null), "html", null, true);
                echo "</span>";
            }
            // line 78
            echo "</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 81
    public function getreadonly($_label = null, $_value = null)
    {
        $context = $this->env->mergeGlobals(array(
            "label" => $_label,
            "value" => $_value,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 82
            echo "
<div class=\"form-group\">
\t<label class=\"control-label\">
\t\t";
            // line 85
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
            echo "
\t</label>
\t<div class=\"controls\">
\t\t<span class=\"input-large uneditable-input\">";
            // line 88
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "</span>
\t</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 93
    public function getslider($_fieldId = null, $_value = null)
    {
        $context = $this->env->mergeGlobals(array(
            "fieldId" => $_fieldId,
            "value" => $_value,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 94
            echo "  <input type=\"text\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" class=\"span6 slider\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "\" 
  \tdata-slider-min=\"1\" data-slider-max=\"100\" 
  \tdata-slider-step=\"1\" 
  \tdata-slider-value=\"";
            // line 97
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            echo "\" 
  \tdata-slider-orientation=\"horizontal\" 
  \tdata-slider-selection=\"before\" />
  \t<span>";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "unit_percent"), "method"), "html", null, true);
            echo "</span>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 103
    public function getteamPlayerSelection($_fieldId = null, $_label = null, $_selectedPlayer = null, $_positionsAndPlayers = null)
    {
        $context = $this->env->mergeGlobals(array(
            "fieldId" => $_fieldId,
            "label" => $_label,
            "selectedPlayer" => $_selectedPlayer,
            "positionsAndPlayers" => $_positionsAndPlayers,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 104
            echo "
<div class=\"form-group\">
\t<label class=\"control-label\" for=\"";
            // line 106
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\">
\t\t";
            // line 107
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
            echo "
\t</label>
\t<div class=\"controls\">
\t\t<select name=\"";
            // line 110
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["fieldId"]) ? $context["fieldId"] : null), "html", null, true);
            echo "\">
\t\t\t<option></option>
\t\t\t";
            // line 112
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["positionsAndPlayers"]) ? $context["positionsAndPlayers"] : null));
            foreach ($context['_seq'] as $context["position"] => $context["players"]) {
                // line 113
                echo "\t\t\t\t<optgroup label=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("player_position_" . (isset($context["position"]) ? $context["position"] : null))), "method"), "html", null, true);
                echo "\">
\t\t\t\t";
                // line 114
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["players"]) ? $context["players"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
                    if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "transfermarket") != "1")) {
                        // line 115
                        echo "\t\t\t\t\t<option value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"), "html", null, true);
                        echo "\" ";
                        if (((isset($context["selectedPlayer"]) ? $context["selectedPlayer"] : null) == $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"))) {
                            echo " selected";
                        }
                        echo ">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "firstname"), "html", null, true);
                        echo " ";
                        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym")) > 0)) {
                            echo "&quot;";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym"), "html", null, true);
                            echo "&quot; ";
                        }
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "lastname"), "html", null, true);
                        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "position_main")) > 0)) {
                            echo " (";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("player_mainposition_" . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "position_main"))), "method"), "html", null, true);
                            echo ")";
                        }
                        echo "</option>
\t\t\t\t";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 117
                echo "\t\t\t\t</optgroup>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['position'], $context['players'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 119
            echo "\t\t</select>
\t</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 124
    public function getsubmitButton($_label = null)
    {
        $context = $this->env->mergeGlobals(array(
            "label" => $_label,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 125
            echo "\t<div class=\"form-group\">
\t\t<div class=\"col-sm-offset-3 col-sm-9\">
\t\t\t<button type=\"submit\" class=\"btn btn-primary\">";
            // line 127
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
            echo "</button>
\t\t</div>
\t</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros/formelements.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  607 => 127,  603 => 125,  592 => 124,  578 => 119,  571 => 117,  543 => 115,  538 => 114,  533 => 113,  529 => 112,  522 => 110,  516 => 107,  512 => 106,  508 => 104,  494 => 103,  481 => 100,  475 => 97,  464 => 94,  452 => 93,  437 => 88,  431 => 85,  426 => 82,  414 => 81,  402 => 78,  395 => 77,  389 => 76,  379 => 75,  376 => 74,  370 => 72,  364 => 70,  362 => 69,  358 => 68,  351 => 67,  347 => 65,  343 => 63,  341 => 62,  338 => 61,  321 => 60,  295 => 52,  285 => 49,  278 => 48,  263 => 47,  250 => 43,  243 => 42,  237 => 41,  221 => 40,  217 => 38,  211 => 36,  205 => 34,  203 => 33,  199 => 32,  192 => 31,  188 => 29,  184 => 27,  182 => 26,  179 => 25,  162 => 24,  149 => 20,  140 => 19,  134 => 18,  105 => 17,  101 => 15,  95 => 13,  89 => 11,  83 => 9,  72 => 6,  68 => 4,  63 => 2,  45 => 1,  37 => 123,  28 => 80,  25 => 59,  22 => 46,  19 => 23,  145 => 48,  136 => 47,  132 => 46,  129 => 45,  121 => 44,  118 => 43,  115 => 42,  112 => 41,  109 => 40,  107 => 39,  104 => 38,  102 => 37,  94 => 32,  87 => 10,  80 => 24,  76 => 8,  71 => 20,  66 => 3,  61 => 17,  56 => 15,  51 => 14,  49 => 13,  43 => 9,  40 => 131,  34 => 102,  31 => 92,  26 => 2,);
    }
}

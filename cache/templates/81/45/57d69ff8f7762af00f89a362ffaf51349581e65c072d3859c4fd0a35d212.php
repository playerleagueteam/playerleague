<?php

/* blocks/nextmatch-profilo.twig */
class __TwigTemplate_814557d69ff8f7762af00f89a362ffaf51349581e65c072d3859c4fd0a35d212 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'nextmatch_footer' => array($this, 'block_nextmatch_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<em>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "next_match_block_title"), "method"), "html", null, true);
        echo "</em>
";
        // line 2
        if (array_key_exists("match_date", $context)) {
            // line 3
            echo "\t<div class=\"span4\">
\t\t<p><em>";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDatetime", array(0 => (isset($context["match_date"]) ? $context["match_date"] : null)), "method"), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("matchtype_" . (isset($context["match_type"]) ? $context["match_type"] : null))), "method"), "html", null, true);
            echo "</em></p>
\t\t<p><strong><a href=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . (isset($context["match_home_id"]) ? $context["match_home_id"] : null))), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["match_home_name"]) ? $context["match_home_name"] : null), "html", null, true);
            echo "</a> 
\t\t\t";
            // line 6
            if ((isset($context["match_home_formation_id"]) ? $context["match_home_formation_id"] : null)) {
                // line 7
                echo "\t\t\t\t<i class=\"icon-check-sign wstooltip\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_submitted_tooltip"), "method"), "html", null, true);
                echo "\"></i>
\t\t\t";
            } else {
                // line 9
                echo "\t\t\t\t<i class=\"icon-minus-sign-alt wstooltip\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_notsubmitted_tooltip"), "method"), "html", null, true);
                echo "\"></i>
\t\t\t";
            }
            // line 10
            echo " 
\t\t\t- 
\t\t\t<a href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . (isset($context["match_guest_id"]) ? $context["match_guest_id"] : null))), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["match_guest_name"]) ? $context["match_guest_name"] : null), "html", null, true);
            echo "</a>
\t\t\t";
            // line 13
            if ((isset($context["match_guest_formation_id"]) ? $context["match_guest_formation_id"] : null)) {
                // line 14
                echo "\t\t\t\t<i class=\"icon-check-sign wstooltip\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_submitted_tooltip"), "method"), "html", null, true);
                echo "\"></i>
\t\t\t";
            } else {
                // line 16
                echo "\t\t\t\t<i class=\"icon-minus-sign-alt wstooltip\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_notsubmitted_tooltip"), "method"), "html", null, true);
                echo "\"></i>
\t\t\t";
            }
            // line 17
            echo " 
\t\t\t</strong></p>
\t\t<p><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "match", 1 => ("id=" . (isset($context["match_id"]) ? $context["match_id"] : null))), "method"), "html", null, true);
            echo "\" class=\"btn\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_navlabel"), "method"), "html", null, true);
            echo "</a></p>
\t</div>
";
        } else {
            // line 22
            echo "<p>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "next_match_block_no_nextmatch"), "method"), "html", null, true);
            echo "</p>
";
        }
        // line 24
        $this->displayBlock('nextmatch_footer', $context, $blocks);
    }

    public function block_nextmatch_footer($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "blocks/nextmatch-profilo.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 24,  92 => 22,  84 => 19,  80 => 17,  74 => 16,  68 => 14,  66 => 13,  60 => 12,  56 => 10,  50 => 9,  44 => 7,  42 => 6,  36 => 5,  30 => 4,  27 => 3,  25 => 2,  20 => 1,);
    }
}

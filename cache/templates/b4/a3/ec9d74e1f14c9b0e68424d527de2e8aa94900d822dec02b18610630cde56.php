<?php

/* blocks/match_result_players.twig */
class __TwigTemplate_b4a3ec9d74e1f14c9b0e68424d527de2e8aa94900d822dec02b18610630cde56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["playerlists"] = $this->env->loadTemplate("macros/playerlists.twig");
        // line 2
        echo "
<div class=\"row-fluid\">
\t<div class=\"span6\">
\t\t<h4>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name"), "html", null, true);
        echo "</h4>
\t\t
\t\t";
        // line 7
        echo $context["playerlists"]->getmatchResultPlayers((isset($context["match"]) ? $context["match"] : null), (isset($context["home_players"]) ? $context["home_players"] : null));
        echo "
\t</div>
\t<div class=\"span6\">
\t\t<h4>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name"), "html", null, true);
        echo "</h4>
\t\t
\t\t";
        // line 12
        echo $context["playerlists"]->getmatchResultPlayers((isset($context["match"]) ? $context["match"] : null), (isset($context["guest_players"]) ? $context["guest_players"] : null));
        echo "
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "blocks/match_result_players.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 12,  37 => 10,  31 => 7,  26 => 5,  21 => 2,  19 => 1,);
    }
}

<?php

/* blocks/shoutbox.twig */
class __TwigTemplate_ca389cafce7e864a070497a8b75e048bea44a0d9a013bf888e4e5009b2f20e9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("box.twig");

        $this->blocks = array(
            'box_title' => array($this, 'block_box_title'),
            'box_content' => array($this, 'block_box_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "box.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_box_title($context, array $blocks = array())
    {
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "shoutbox_title"), "method"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_box_content($context, array $blocks = array())
    {
        // line 8
        echo "<div id=\"shoutboxvalmessages\"></div>

<div style=\"text-align: right;\">
\t<a href=\"#shrefresh\" id=\"shrefresh\" class=\"btn btn-mini ajaxLink\" 
\t\tdata-ajaxtarget=\"shoutbox_block\" data-ajaxblock=\"shoutbox\" data-ajaxquerystr=\"id=";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method"), "html", null, true);
        echo "\"
\t\tstyle=\"margin-bottom: 5px;\"><i class=\"icon-refresh\"></i> ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_refresh"), "method"), "html", null, true);
        echo "</a>
</div>
<div class=\"shoutboxmessages\">

\t";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) ? $context["messages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 18
            echo "\t<div>
\t\t";
            // line 19
            if ($this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "isAdmin", array(), "method")) {
                echo "<a href=\"#\" class=\"ajaxLink\" 
\t\tdata-ajaxtarget=\"shoutbox_block\" data-ajaxblock=\"shoutbox\" data-ajaxquerystr=\"id=";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method"), "html", null, true);
                echo "&action=delete-shoutbox-message&mid=";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "message_id"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_delete"), "method"), "html", null, true);
                echo "\"><i class=\"icon-trash\"></i></a>";
            }
            echo " <small class=\"muted\" style=\"font-size: 9px\">[";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDatetime", array(0 => $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "date"), 1 => (isset($context["i18n"]) ? $context["i18n"] : null)), "method"), "html", null, true);
            echo "] <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "match", 1 => ("id=" . $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "match_id"))), "method"), "html", null, true);
            echo "\" class=\"muted\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "home_name"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "guest_name"), "html", null, true);
            echo "</a></small>
\t\t<br>
\t\t<a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "user", 1 => ("id=" . $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "user_id"))), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "user_name"), "html", null, true);
            echo "</a>: ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "message"), "html", null, true);
            echo "
\t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "
</div>
";
        // line 27
        if (($this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "id") && ((!array_key_exists("hidesubmit", $context)) || (!(isset($context["hidesubmit"]) ? $context["hidesubmit"] : null))))) {
            // line 28
            echo "<form class=\"form form-inline\" method=\"post\">
\t<input type=\"hidden\" name=\"action\" value=\"send-shoutbox-msg\"/>
\t<input type=\"hidden\" name=\"block\" value=\"shoutbox\"/>
\t<input type=\"hidden\" name=\"id\" value=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method"), "html", null, true);
            echo "\"/>
\t
\t<input class=\"span3\" id=\"msgtext\" name=\"msgtext\" type=\"text\" placeholder=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "shoutbox_messageinput_placeholder"), "method"), "html", null, true);
            echo "\" required=\"true\" 
\t\tvalue=\"";
            // line 34
            if ($this->getAttribute((isset($context["validationMsg"]) ? $context["validationMsg"] : null), "msgtext", array(), "array", true, true)) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "msgtext"), "method"), "html", null, true);
            }
            echo "\">
\t<input type=\"submit\" class=\"btn ajaxSubmit\"
\t\tdata-ajaxtarget=\"shoutbox_block\" data-messagetarget=\"shoutboxvalmessages\" value=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "shoutbox_button_go"), "method"), "html", null, true);
            echo "\"/>
\t\t
\t";
            // line 38
            if ($this->getAttribute((isset($context["validationMsg"]) ? $context["validationMsg"] : null), "msgtext", array(), "array", true, true)) {
                echo "<p class=\"text-error\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["validationMsg"]) ? $context["validationMsg"] : null), "msgtext", array(), "array"), "html", null, true);
                echo "</p>";
            }
            // line 39
            echo "</form>
";
        } else {
            // line 41
            echo "\t<p><small>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "shoutbox_hiddensubmit_note"), "method"), "html", null, true);
            echo "</small></p>
";
        }
        // line 43
        echo "
";
    }

    public function getTemplateName()
    {
        return "blocks/shoutbox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 43,  143 => 41,  139 => 39,  133 => 38,  121 => 34,  117 => 33,  112 => 31,  105 => 27,  88 => 22,  65 => 19,  62 => 18,  58 => 17,  51 => 13,  41 => 8,  32 => 4,  29 => 3,  24 => 3,  19 => 1,  46 => 5,  38 => 7,  34 => 2,  21 => 2,  544 => 211,  540 => 209,  534 => 207,  532 => 206,  522 => 198,  517 => 195,  511 => 194,  508 => 193,  505 => 192,  502 => 191,  500 => 190,  493 => 187,  487 => 185,  485 => 184,  482 => 183,  476 => 181,  469 => 180,  462 => 179,  460 => 178,  456 => 176,  452 => 174,  448 => 172,  444 => 170,  440 => 168,  436 => 166,  432 => 164,  428 => 162,  424 => 160,  420 => 158,  416 => 156,  414 => 155,  410 => 153,  407 => 152,  405 => 151,  400 => 149,  396 => 147,  390 => 143,  386 => 141,  380 => 138,  376 => 136,  373 => 135,  370 => 133,  365 => 132,  363 => 131,  360 => 130,  354 => 127,  350 => 125,  348 => 124,  339 => 117,  333 => 115,  331 => 114,  321 => 111,  313 => 110,  300 => 104,  296 => 103,  286 => 100,  282 => 99,  276 => 96,  271 => 93,  267 => 91,  259 => 89,  257 => 88,  252 => 86,  247 => 84,  244 => 83,  242 => 82,  237 => 79,  231 => 77,  228 => 76,  222 => 74,  220 => 73,  213 => 68,  196 => 66,  192 => 65,  188 => 63,  171 => 61,  167 => 60,  162 => 57,  154 => 56,  150 => 55,  144 => 53,  136 => 52,  128 => 36,  122 => 45,  119 => 44,  116 => 43,  113 => 42,  110 => 41,  107 => 28,  101 => 25,  99 => 38,  93 => 35,  84 => 29,  77 => 24,  72 => 22,  69 => 20,  67 => 20,  64 => 19,  60 => 17,  56 => 15,  52 => 13,  50 => 12,  47 => 12,  44 => 10,  36 => 7,  33 => 6,  28 => 4,  26 => 3,);
    }
}

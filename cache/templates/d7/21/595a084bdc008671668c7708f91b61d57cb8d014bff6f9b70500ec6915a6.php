<?php

/* views/match-details.twig */
class __TwigTemplate_d721595a084bdc008671668c7708f91b61d57cb8d014bff6f9b70500ec6915a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["profile"] = $this->env->loadTemplate("macros/profileelements.twig");
        // line 4
        $context["msgbox"] = $this->env->loadTemplate("macros/messagebox.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_page_title($context, array $blocks = array())
    {
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name"), "html", null, true);
        echo "
";
    }

    // line 10
    public function block_page_content($context, array $blocks = array())
    {
        // line 11
        echo "
";
        // line 12
        if (($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_type") == "league")) {
            // line 13
            echo "\t";
            $context["matchtype"] = (($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("matchtype_" . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_type"))), "method") . ", ") . $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_matchday", 1 => $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_matchday")), "method"));
        } elseif (($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_type") == "cup")) {
            // line 15
            echo "\t";
            $context["matchtype"] = ((((($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("matchtype_" . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_type"))), "method") . ", ") . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_cup_name")) . " (") . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_cup_round")) . ")");
        } else {
            // line 17
            echo "\t";
            $context["matchtype"] = $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("matchtype_" . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_type"))), "method");
        }
        // line 19
        echo "
";
        // line 20
        if ((($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_simulated") == 0) && ($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_minutes") < 1))) {
            // line 21
            echo "\t<hr/>
\t";
            // line 22
            echo $context["msgbox"]->getbox("", $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_notyetsimulated"), "method"), "info");
            echo "
";
        } else {
            // line 24
            echo "
<div style=\"background-color: #484848;color: #FFF;margin-bottom: -10px;\">
<ul>

\t<li style=\"display: inline;\">
\t\t";
            // line 29
            echo twig_escape_filter($this->env, (isset($context["matchtype"]) ? $context["matchtype"] : null), "html", null, true);
            echo "
\t</li>
\t<li style=\"
\tdisplay: inline;
\tmargin-left: 15%;
    margin-right: 15%;\">
\t\t";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDatetime", array(0 => $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_date"), 1 => (isset($context["i18n"]) ? $context["i18n"] : null)), "method"), "html", null, true);
            echo "
\t</li>
\t<li style=\"display: inline;float:right\">
\t";
            // line 38
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_stadium_name")) > 0)) {
                // line 39
                echo "\t";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_stadium_name"), "html", null, true);
                echo "
\t";
            }
            // line 40
            echo " (";
            if (($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_audience") > 0)) {
                // line 41
                echo "\t";
                $context["audience"] = twig_number_format_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_audience"), 0, ",", " ");
                // line 42
                echo "\t";
                if ($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_soldout")) {
                    // line 43
                    echo "\t\t";
                    $context["audience"] = ((((isset($context["audience"]) ? $context["audience"] : null) . " (") . $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_match_soldout"), "method")) . ")");
                    // line 44
                    echo "\t";
                }
                // line 45
                echo "\t";
                echo twig_escape_filter($this->env, (isset($context["audience"]) ? $context["audience"] : null), "html", null, true);
                echo "
";
            }
            // line 46
            echo ")
\t</li>
</ul>
</div>
\t<div id=\"report_result\" style=\"background-image: url('http://www.thesportsroadtrip.com/clipart/images/sidebars/grayDotTexture.jpg'); background-repeat: repeat;\">
\t\t<span id=\"report_goals_home\" style=\"font-family: 'risultato-match';    font-size: 55px;\tcolor: #FFD705;\">
\t\t";
            // line 52
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_picture"))) {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
                echo "/uploads/club/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_picture"), "html", null, true);
                echo "\" style=\"max-height: 120px; margin-right: 20px\"/>";
            }
            // line 53
            echo "\t\t";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_goals_home"), "html", null, true);
            echo "</span>
\t\t<span id=\"report_goals_separator\" style=\"font-family: 'risultato-match';    font-size: 55px;\tcolor: #FFD705;\">-</span>
\t\t<span id=\"report_goals_guest\" style=\"font-family: 'risultato-match';    font-size: 55px;\tcolor: #FFD705;\">";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_goals_guest"), "html", null, true);
            echo "
\t\t";
            // line 56
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_picture"))) {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
                echo "/uploads/club/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_picture"), "html", null, true);
                echo "\" style=\"max-height: 120px; margin-left: 20px\"/>";
            }
            // line 57
            echo "\t\t</span>
\t\t<div class=\"row\" style=\"margin: -10px 0 10px 0\">
\t\t\t<div style=\"width: 50%; float: left;\">
\t\t\t\t";
            // line 60
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["homeStrikerMessages"]) ? $context["homeStrikerMessages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["goalMessage"]) {
                // line 61
                echo "\t\t\t\t<small style=\"font-weight: bold;color: #fff;\">";
                echo twig_escape_filter($this->env, $this->getAttribute(twig_split_filter($this->getAttribute((isset($context["goalMessage"]) ? $context["goalMessage"] : null), "playerNames"), ";"), 0, array(), "array"), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["goalMessage"]) ? $context["goalMessage"] : null), "minute"), "html", null, true);
                echo ".";
                if (($this->getAttribute((isset($context["goalMessage"]) ? $context["goalMessage"] : null), "type") == "Elfmeter_erfolg")) {
                    echo ", ";
                    echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_commenttitle_penalty"), "method"), 0, 1), "html", null, true);
                    echo ".";
                }
                echo ")<br></small>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['goalMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "\t\t\t</div>
\t\t\t<div style=\"width: 50%; float: right;\">
\t\t\t\t";
            // line 65
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["guestStrikerMessages"]) ? $context["guestStrikerMessages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["goalMessage"]) {
                // line 66
                echo "\t\t\t\t<small style=\"font-weight: bold;color: #fff;\">";
                echo twig_escape_filter($this->env, $this->getAttribute(twig_split_filter($this->getAttribute((isset($context["goalMessage"]) ? $context["goalMessage"] : null), "playerNames"), ";"), 0, array(), "array"), "html", null, true);
                echo " (";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["goalMessage"]) ? $context["goalMessage"] : null), "minute"), "html", null, true);
                echo ".";
                if (($this->getAttribute((isset($context["goalMessage"]) ? $context["goalMessage"] : null), "type") == "Elfmeter_erfolg")) {
                    echo ", ";
                    echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_commenttitle_penalty"), "method"), 0, 1), "html", null, true);
                    echo ".";
                }
                echo ")<br></small>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['goalMessage'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "\t\t\t</div>
\t\t</div>
\t</div>

\t<div style=\"margin-top: 10px\">
\t";
            // line 73
            if ($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_noformation")) {
                // line 74
                echo "\t";
                echo $context["msgbox"]->getbox("", $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_noformation", 1 => $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name")), "method"), "warning");
                echo "
\t";
            }
            // line 76
            echo "\t";
            if ($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_noformation")) {
                // line 77
                echo "\t";
                echo $context["msgbox"]->getbox("", $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_noformation", 1 => $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name")), "method"), "warning");
                echo "
\t";
            }
            // line 79
            echo "\t</div>

\t<p style=\"margin-top: 10px\">
\t";
            // line 82
            if ((!$this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_simulated"))) {
                // line 83
                echo "\t\t<a id=\"matchReportRefresh\" class=\"btn ajaxLink\" data-ajaxtarget=\"pagecontent\"
\t\t\tdata-ajaxquerystr=\"page=match&amp;id=";
                // line 84
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_id"), "html", null, true);
                echo "\"
\t\t\tdata-blockseconds=\"60\"
\t\t\thref=\"#refresh\"><i class=\"icon-refresh\"></i> ";
                // line 86
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_refresh"), "method"), "html", null, true);
                echo " <span class=\"timerCount\"></span></a>

\t\t";
                // line 88
                if ((isset($context["allowTacticChanges"]) ? $context["allowTacticChanges"] : null)) {
                    // line 89
                    echo "\t\t\t<a class=\"btn\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "match-live-changes", 1 => ("id=" . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_id"))), "method"), "html", null, true);
                    echo "\"><i class=\"icon-edit\"></i> ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_button_changes"), "method"), "html", null, true);
                    echo "</a>
\t\t";
                }
                // line 91
                echo "
\t";
            }
            // line 93
            echo "\t</p>

\t<ul class=\"nav nav-tabs\" id=\"reportTab\">
\t  <li class=\"active\"><a href=\"#report\" data-toggle=\"tab\">";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_comments_title"), "method"), "html", null, true);
            echo "</a></li>
\t  <li><a href=\"#players\" data-toggle=\"tab\" class=\"ajaxLink\"
\t  \tdata-ajaxtarget=\"players-list\" data-ajaxblock=\"match-result-players\"
\t  \tdata-ajaxquerystr=\"id=";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_id"), "html", null, true);
            echo "\"
\t  \t";
            // line 100
            if ((!$this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_simulated"))) {
                echo "data-ajaxdisabledcache=\"1\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_players_title"), "method"), "html", null, true);
            echo "</a></li>
\t  <li><a href=\"#statistic\" data-toggle=\"tab\" class=\"ajaxLink\"
\t  \tdata-ajaxtarget=\"matchstatistic\" data-ajaxblock=\"match-statistics\"
\t  \tdata-ajaxquerystr=\"id=";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_id"), "html", null, true);
            echo "\"
\t  \t";
            // line 104
            if ((!$this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_simulated"))) {
                echo "data-ajaxdisabledcache=\"1\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_statistic_title"), "method"), "html", null, true);
            echo "</a></li>
\t</ul>

\t<div class=\"tab-content\">
\t  <div class=\"tab-pane active\" id=\"report\">

\t  \t<span class=\"report_team_home\">";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name"), "html", null, true);
            echo " <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_id"))), "method"), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "team_navlabel"), "method"), "html", null, true);
            echo "\"><i class=\"icon-info-sign darkIcon\"></i></a></span>
\t  \t<span style=\"margin-left: 20px\" class=\"report_team_guest\">";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name"), "html", null, true);
            echo " <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_id"))), "method"), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "team_navlabel"), "method"), "html", null, true);
            echo "\"><i class=\"icon-info-sign darkIcon\"></i></a></span>

\t\t<div id=\"reportarea\">
\t\t";
            // line 114
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_deprecated_report")) > 0)) {
                // line 115
                echo "\t\t\t";
                echo nl2br($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_deprecated_report"));
                echo "
\t\t";
            } else {
                // line 117
                echo "
\t\t\t<table id=\"reporttable\">
\t\t\t\t<colgroup>
\t\t\t\t\t<col style=\"width: 60px\" />
\t\t\t\t\t<col />
\t\t\t\t</colgroup>
\t\t\t\t<tbody>
\t\t\t\t\t";
                // line 124
                if ($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_simulated")) {
                    // line 125
                    echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t<td style=\"text-align: center\"><i class=\"websoccericon-matchcompleted\"></i></td>
\t\t\t\t\t\t<td><h4>";
                    // line 127
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_match_completed"), "method"), "html", null, true);
                    echo "</h4></td>
\t\t\t\t\t</tr>
\t\t\t\t\t";
                }
                // line 130
                echo "
\t\t\t\t\t";
                // line 131
                $context["previousMinute"] = 0;
                // line 132
                echo "\t\t\t\t\t";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reportmessages"]) ? $context["reportmessages"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["reportmessage"]) {
                    // line 133
                    echo "
\t\t\t\t\t\t";
                    // line 135
                    echo "\t\t\t\t\t\t";
                    if ((((isset($context["previousMinute"]) ? $context["previousMinute"] : null) > 45) && ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "minute") <= 45))) {
                        // line 136
                        echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td></td>
\t\t\t\t\t\t\t\t<td style=\"background-color: #FFD3D3\"><h4>";
                        // line 138
                        echo twig_escape_filter($this->env, sprintf($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_report_halftime"), "method"), $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "goals")), "html", null, true);
                        echo "</h4></td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
                    } elseif (((($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_penalty_enabled") || ($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_type") == "cup")) && ((isset($context["previousMinute"]) ? $context["previousMinute"] : null) > 91)) && ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "minute") <= 91))) {
                        // line 141
                        echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td></td>
\t\t\t\t\t\t\t\t<td><h4>";
                        // line 143
                        echo twig_escape_filter($this->env, sprintf($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_report_extension"), "method"), $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "goals")), "html", null, true);
                        echo "</h4></td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
                    } elseif ((((array_key_exists("displayPenaltyMsg", $context) && ((isset($context["displayPenaltyMsg"]) ? $context["displayPenaltyMsg"] : null) == 1)) && ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") != "Elfmeter_erfolg")) && ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") != "Elfmeter_verschossen"))) {
                        // line 147
                        echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td></td>
\t\t\t\t\t\t\t\t<td><h4>";
                        // line 149
                        echo twig_escape_filter($this->env, sprintf($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_report_penaltyshooting"), "method"), $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "goals")), "html", null, true);
                        echo "</h4></td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
                        // line 151
                        $context["displayPenaltyMsg"] = 0;
                        // line 152
                        echo "\t\t\t\t\t\t";
                    }
                    // line 153
                    echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"text-align: center\">
\t\t\t\t\t\t\t\t";
                    // line 155
                    if ((((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Tor") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_erfolg")) || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Tor_mit_vorlage")) || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Freistoss_treffer"))) {
                        // line 156
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-goal\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Karte_gelb")) {
                        // line 158
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-yellowcard\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif ((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Karte_gelb_rot") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Karte_rot"))) {
                        // line 160
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-redcard\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Auswechslung")) {
                        // line 162
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-substitution\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Verletzung")) {
                        // line 164
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-injury\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Torschuss_daneben")) {
                        // line 166
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-shoot\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Torschuss_auf_Tor")) {
                        // line 168
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-attempt\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif ((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_verschossen") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Freistoss_daneben"))) {
                        // line 170
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-penalty\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Ecke")) {
                        // line 172
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-corner\"></i>
\t\t\t\t\t\t\t\t";
                    } elseif (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Taktikaenderung")) {
                        // line 174
                        echo "\t\t\t\t\t\t\t\t\t<i class=\"websoccericon-tacticschange\"></i>
\t\t\t\t\t\t\t\t";
                    }
                    // line 176
                    echo "\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t";
                    // line 178
                    $context["teamMarkerClass"] = (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "active_home")) ? ("report_team_home") : ("report_team_guest"));
                    // line 179
                    echo "\t\t\t\t\t\t\t\t\t<div class=\"report_minute ";
                    echo twig_escape_filter($this->env, (isset($context["teamMarkerClass"]) ? $context["teamMarkerClass"] : null), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_report_minute", 1 => $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "minute")), "method"), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t";
                    // line 180
                    if ((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_erfolg") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_verschossen"))) {
                        echo " - ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_commenttitle_penalty"), "method"), "html", null, true);
                        echo "
\t\t\t\t\t\t\t\t\t";
                    } elseif ((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Tor") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Tor_mit_vorlage"))) {
                        // line 181
                        echo " - ";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_commenttitle_goal"), "method"), "html", null, true);
                        echo "
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 183
                    echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    // line 184
                    if ((((((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_erfolg") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_verschossen")) || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Tor")) || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Tor_mit_vorlage")) || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Freistoss_treffer")) && (twig_length_filter($this->env, $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "goals")) > 0))) {
                        // line 185
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"report_comment\">";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "goals"), "html", null, true);
                        echo "</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 187
                    echo "\t\t\t\t\t\t\t\t\t<div class=\"report_comment\">";
                    echo $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "message");
                    echo "</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
                    // line 190
                    $context["previousMinute"] = $this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "minute");
                    // line 191
                    echo "\t\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "minute") == 121) && (($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_erfolg") || ($this->getAttribute((isset($context["reportmessage"]) ? $context["reportmessage"] : null), "type") == "Elfmeter_verschossen")))) {
                        // line 192
                        echo "\t\t\t\t\t\t\t";
                        $context["displayPenaltyMsg"] = 1;
                        // line 193
                        echo "\t\t\t\t\t\t";
                    }
                    // line 194
                    echo "\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reportmessage'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 195
                echo "\t\t\t\t</tbody>
\t\t\t</table>
\t\t";
            }
            // line 198
            echo "
\t\t</div>
\t  </div>
\t  <div class=\"tab-pane\" id=\"players\">
\t\t<span id=\"players-list\"></span>
\t  </div>
\t  <div class=\"tab-pane\" id=\"statistic\">

\t\t";
            // line 206
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_deprecated_report")) > 0)) {
                // line 207
                echo "\t\t\t<p>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_nostatistics_available"), "method"), "html", null, true);
                echo "</p>
\t\t";
            } else {
                // line 209
                echo "\t\t\t<span id=\"matchstatistic\"></span>
\t\t";
            }
            // line 211
            echo "
\t  </div>
\t</div>

";
        }
    }

    public function getTemplateName()
    {
        return "views/match-details.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  544 => 211,  540 => 209,  534 => 207,  532 => 206,  522 => 198,  517 => 195,  511 => 194,  508 => 193,  505 => 192,  502 => 191,  500 => 190,  493 => 187,  487 => 185,  485 => 184,  482 => 183,  476 => 181,  469 => 180,  462 => 179,  460 => 178,  456 => 176,  452 => 174,  448 => 172,  444 => 170,  440 => 168,  436 => 166,  432 => 164,  428 => 162,  424 => 160,  420 => 158,  416 => 156,  414 => 155,  410 => 153,  407 => 152,  405 => 151,  400 => 149,  396 => 147,  390 => 143,  386 => 141,  380 => 138,  376 => 136,  373 => 135,  370 => 133,  365 => 132,  363 => 131,  360 => 130,  354 => 127,  350 => 125,  348 => 124,  339 => 117,  333 => 115,  331 => 114,  321 => 111,  313 => 110,  300 => 104,  296 => 103,  286 => 100,  282 => 99,  276 => 96,  271 => 93,  267 => 91,  259 => 89,  257 => 88,  252 => 86,  247 => 84,  244 => 83,  242 => 82,  237 => 79,  231 => 77,  228 => 76,  222 => 74,  220 => 73,  213 => 68,  196 => 66,  192 => 65,  188 => 63,  171 => 61,  167 => 60,  162 => 57,  154 => 56,  150 => 55,  144 => 53,  136 => 52,  128 => 46,  122 => 45,  119 => 44,  116 => 43,  113 => 42,  110 => 41,  107 => 40,  101 => 39,  99 => 38,  93 => 35,  84 => 29,  77 => 24,  72 => 22,  69 => 21,  67 => 20,  64 => 19,  60 => 17,  56 => 15,  52 => 13,  50 => 12,  47 => 11,  44 => 10,  36 => 7,  33 => 6,  28 => 4,  26 => 3,);
    }
}

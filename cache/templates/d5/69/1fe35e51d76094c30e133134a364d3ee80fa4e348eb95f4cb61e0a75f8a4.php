<?php

/* macros/paginator.twig */
class __TwigTemplate_d5691fe35e51d76094c30e133134a364d3ee80fa4e348eb95f4cb61e0a75f8a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    // line 1
    public function getpaginator($_paginatorModel = null, $_ajaxTarget = "pagecontent")
    {
        $context = $this->env->mergeGlobals(array(
            "paginatorModel" => $_paginatorModel,
            "ajaxTarget" => $_ajaxTarget,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            if (($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pages") > 1)) {
                // line 3
                echo "<div class=\"pagination\">
  <ul>
  \t";
                // line 5
                if (($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") > 1)) {
                    // line 6
                    echo "    <li><a href=\"#p";
                    echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") - 1), "html", null, true);
                    echo "\"
    \tclass=\"ajaxLink\" data-ajaxtarget=\"";
                    // line 7
                    echo twig_escape_filter($this->env, (isset($context["ajaxTarget"]) ? $context["ajaxTarget"] : null), "html", null, true);
                    echo "\" data-ajaxquerystr=\"page=";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getPageId", array(), "method"), "html", null, true);
                    echo "&";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "getQueryString", array(), "method"), "html", null, true);
                    echo "&pageno=";
                    echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") - 1), "html", null, true);
                    echo "\">&laquo;</a></li>
    ";
                }
                // line 9
                echo "    
    ";
                // line 10
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pages")));
                foreach ($context['_seq'] as $context["_key"] => $context["pageNo"]) {
                    // line 11
                    echo "    
    ";
                    // line 12
                    if ((((isset($context["pageNo"]) ? $context["pageNo"] : null) == ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") - 5)) || ((isset($context["pageNo"]) ? $context["pageNo"] : null) == ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") + 5)))) {
                        // line 13
                        echo "    <li class=\"disabled\"><a>...</a></li>
    ";
                    } elseif (((((isset($context["pageNo"]) ? $context["pageNo"] : null) == 1) || ((isset($context["pageNo"]) ? $context["pageNo"] : null) == $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pages"))) || (((isset($context["pageNo"]) ? $context["pageNo"] : null) > ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") - 5)) && ((isset($context["pageNo"]) ? $context["pageNo"] : null) < ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") + 5))))) {
                        // line 15
                        echo "    <li";
                        if (((isset($context["pageNo"]) ? $context["pageNo"] : null) == $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo"))) {
                            echo " class=\"active\"";
                        }
                        echo "><a href=\"#p";
                        echo twig_escape_filter($this->env, (isset($context["pageNo"]) ? $context["pageNo"] : null), "html", null, true);
                        echo "\"
    \tclass=\"ajaxLink\" data-ajaxtarget=\"";
                        // line 16
                        echo twig_escape_filter($this->env, (isset($context["ajaxTarget"]) ? $context["ajaxTarget"] : null), "html", null, true);
                        echo "\" data-ajaxquerystr=\"page=";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getPageId", array(), "method"), "html", null, true);
                        echo "&";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "getQueryString", array(), "method"), "html", null, true);
                        echo "&pageno=";
                        echo twig_escape_filter($this->env, (isset($context["pageNo"]) ? $context["pageNo"] : null), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, (isset($context["pageNo"]) ? $context["pageNo"] : null), "html", null, true);
                        echo "</a></li>
    ";
                    }
                    // line 18
                    echo "    
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pageNo'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 20
                echo "    
    ";
                // line 21
                if (($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") < $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pages"))) {
                    // line 22
                    echo "    <li><a href=\"#";
                    echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") + 1), "html", null, true);
                    echo "\"
    \tclass=\"ajaxLink\" data-ajaxtarget=\"";
                    // line 23
                    echo twig_escape_filter($this->env, (isset($context["ajaxTarget"]) ? $context["ajaxTarget"] : null), "html", null, true);
                    echo "\" data-ajaxquerystr=\"page=";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getPageId", array(), "method"), "html", null, true);
                    echo "&";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "getQueryString", array(), "method"), "html", null, true);
                    echo "&pageno=";
                    echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["paginatorModel"]) ? $context["paginatorModel"] : null), "pageNo") + 1), "html", null, true);
                    echo "\">&raquo;</a></li>
    ";
                }
                // line 25
                echo "  </ul>
</div>
";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros/paginator.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 25,  107 => 22,  105 => 21,  95 => 18,  73 => 15,  69 => 13,  64 => 11,  60 => 10,  41 => 6,  39 => 5,  35 => 3,  33 => 2,  21 => 1,  158 => 52,  153 => 49,  145 => 46,  141 => 45,  134 => 44,  130 => 43,  126 => 41,  120 => 39,  114 => 37,  112 => 23,  108 => 34,  102 => 20,  96 => 30,  94 => 29,  89 => 27,  86 => 26,  82 => 16,  75 => 21,  71 => 20,  67 => 12,  63 => 18,  57 => 9,  55 => 13,  46 => 7,  43 => 10,  40 => 9,  34 => 6,  31 => 5,  26 => 3,);
    }
}

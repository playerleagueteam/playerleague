<?php

/* macros/messagebox.twig */
class __TwigTemplate_0b25661698c4ddb21b4320dd913b8a3ca4d9516c9a960ae79fabda4b5576b51c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    // line 1
    public function getbox($_title = null, $_message = null, $_type = null)
    {
        $context = $this->env->mergeGlobals(array(
            "title" => $_title,
            "message" => $_message,
            "type" => $_type,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            $context["alertClass"] = ((((isset($context["type"]) ? $context["type"] : null) == "error")) ? ("danger") : ((isset($context["type"]) ? $context["type"] : null)));
            // line 3
            echo "<div class=\"alert alert-block alert-";
            echo twig_escape_filter($this->env, ((array_key_exists("alertClass", $context)) ? (_twig_default_filter((isset($context["alertClass"]) ? $context["alertClass"] : null), "error")) : ("error")), "html", null, true);
            echo "\">
\t";
            // line 4
            if ((twig_length_filter($this->env, (isset($context["message"]) ? $context["message"] : null)) > 0)) {
                // line 5
                echo "\t  <h4>";
                echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
                echo "</h4>
\t  ";
                // line 6
                echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
                echo "
\t";
            } else {
                // line 8
                echo "\t\t<strong>";
                echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
                echo "</strong>
\t";
            }
            // line 10
            echo "</div>

";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros/messagebox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  48 => 6,  43 => 5,  36 => 3,  34 => 2,  21 => 1,  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 75,  183 => 72,  180 => 71,  171 => 69,  166 => 68,  164 => 67,  145 => 51,  139 => 47,  129 => 44,  124 => 43,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 34,  96 => 33,  93 => 32,  86 => 27,  78 => 25,  75 => 24,  72 => 23,  69 => 22,  66 => 21,  64 => 20,  59 => 10,  55 => 17,  49 => 13,  47 => 12,  44 => 11,  41 => 4,  35 => 7,  32 => 6,  27 => 3,);
    }
}

<?php

/* blocks/notifications.twig */
class __TwigTemplate_3c88700a048148a22891bb095c11d5789934445e97d3f52de2d5c5b57e82f3d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["notifications"]) ? $context["notifications"] : null)) == 0)) {
            // line 2
            echo "\t";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_notifications_noneavailable"), "method"), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "
<ul class=\"unstyled\">
\t";
            // line 6
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["notifications"]) ? $context["notifications"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["notification"]) {
                // line 7
                echo "\t\t<li>
\t\t\t";
                // line 8
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["notification"]) ? $context["notification"] : null), "link")) > 0)) {
                    // line 9
                    echo "\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notification"]) ? $context["notification"] : null), "link"), "html", null, true);
                    echo "\">";
                    echo $this->getAttribute((isset($context["notification"]) ? $context["notification"] : null), "message");
                    echo "</a>
\t\t\t";
                } else {
                    // line 11
                    echo "\t\t\t\t<span>";
                    echo $this->getAttribute((isset($context["notification"]) ? $context["notification"] : null), "message");
                    echo "</span>
\t\t\t";
                }
                // line 13
                echo "\t\t\t<br>
\t\t\t<small class=\"";
                // line 14
                if (($this->getAttribute((isset($context["notification"]) ? $context["notification"] : null), "seen") == 0)) {
                    echo "text-error";
                } else {
                    echo "muted";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDateTime", array(0 => $this->getAttribute((isset($context["notification"]) ? $context["notification"] : null), "eventdate")), "method"), "html", null, true);
                echo "</small>
\t\t</li>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['notification'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "</ul>

";
        }
    }

    public function getTemplateName()
    {
        return "blocks/notifications.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 17,  57 => 14,  54 => 13,  48 => 11,  40 => 9,  38 => 8,  35 => 7,  31 => 6,  27 => 4,  21 => 2,  19 => 1,);
    }
}

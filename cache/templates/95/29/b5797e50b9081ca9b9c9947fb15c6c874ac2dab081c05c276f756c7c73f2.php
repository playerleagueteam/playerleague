<?php

/* macros/statisticelements.twig */
class __TwigTemplate_9529b5797e50b9081ca9b9c9947fb15c6c874ac2dab081c05c276f756c7c73f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 15
        echo "
";
    }

    // line 1
    public function getprogressbar($_percentvalue = 0, $_cssClass = "")
    {
        $context = $this->env->mergeGlobals(array(
            "percentvalue" => $_percentvalue,
            "cssClass" => $_cssClass,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            if (((isset($context["percentvalue"]) ? $context["percentvalue"] : null) > 80)) {
                // line 3
                echo "\t";
                $context["progress_status"] = "success";
            } elseif (((isset($context["percentvalue"]) ? $context["percentvalue"] : null) > 50)) {
                // line 5
                echo "\t";
                $context["progress_status"] = "info";
            } elseif (((isset($context["percentvalue"]) ? $context["percentvalue"] : null) > 30)) {
                // line 7
                echo "\t";
                $context["progress_status"] = "warning";
            } else {
                // line 9
                echo "\t";
                $context["progress_status"] = "danger";
            }
            // line 11
            echo "<div class=\"progress progress-";
            echo twig_escape_filter($this->env, (isset($context["progress_status"]) ? $context["progress_status"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cssClass"]) ? $context["cssClass"] : null), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, (isset($context["percentvalue"]) ? $context["percentvalue"] : null), "html", null, true);
            echo "%\">
  <div class=\"bar\" style=\"width: ";
            // line 12
            echo twig_escape_filter($this->env, (isset($context["percentvalue"]) ? $context["percentvalue"] : null), "html", null, true);
            echo "%\">";
            echo twig_escape_filter($this->env, (isset($context["percentvalue"]) ? $context["percentvalue"] : null), "html", null, true);
            echo "%</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 16
    public function getpiechart($_label1 = null, $_percentvalue1 = null, $_label2 = null, $_percentvalue2 = null)
    {
        $context = $this->env->mergeGlobals(array(
            "label1" => $_label1,
            "percentvalue1" => $_percentvalue1,
            "label2" => $_label2,
            "percentvalue2" => $_percentvalue2,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 17
            echo "
<div class=\"pieChart\" style=\"width:250px;height:250px\" data-series='[{\"label\": \"";
            // line 18
            echo twig_escape_filter($this->env, trim((isset($context["label2"]) ? $context["label2"] : null)), "html", null, true);
            echo "\",  \"data\": \"";
            echo twig_escape_filter($this->env, (isset($context["percentvalue2"]) ? $context["percentvalue2"] : null), "html", null, true);
            echo "\", \"color\": \"#FF6600\"},
{ \"label\": \"";
            // line 19
            echo twig_escape_filter($this->env, trim((isset($context["label1"]) ? $context["label1"] : null)), "html", null, true);
            echo "\",  \"data\": \"";
            echo twig_escape_filter($this->env, (isset($context["percentvalue1"]) ? $context["percentvalue1"] : null), "html", null, true);
            echo "\", \"color\": \"#006699\"}]'></div>
<div class=\"pieChartLabel\"></div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros/statisticelements.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 19,  63 => 12,  50 => 9,  24 => 1,  268 => 86,  262 => 85,  256 => 81,  247 => 79,  242 => 78,  240 => 77,  237 => 76,  228 => 74,  224 => 73,  221 => 72,  203 => 68,  189 => 61,  168 => 53,  159 => 49,  155 => 47,  153 => 46,  149 => 45,  146 => 44,  144 => 43,  137 => 41,  133 => 40,  113 => 33,  100 => 30,  88 => 27,  85 => 26,  77 => 23,  70 => 21,  51 => 12,  42 => 5,  26 => 3,  22 => 2,  67 => 20,  29 => 3,  31 => 5,  23 => 4,  110 => 32,  97 => 33,  92 => 28,  83 => 28,  61 => 21,  54 => 11,  45 => 10,  33 => 12,  19 => 15,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 69,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 57,  173 => 56,  170 => 40,  163 => 51,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 16,  74 => 20,  71 => 25,  62 => 13,  58 => 16,  52 => 19,  46 => 7,  40 => 10,  38 => 3,  30 => 4,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 2,  34 => 6,  21 => 3,  215 => 85,  210 => 82,  200 => 67,  195 => 77,  190 => 76,  188 => 49,  183 => 60,  180 => 44,  171 => 69,  166 => 52,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 29,  96 => 18,  93 => 17,  86 => 27,  78 => 25,  75 => 26,  72 => 22,  69 => 22,  66 => 23,  64 => 19,  59 => 10,  55 => 17,  49 => 11,  47 => 11,  44 => 11,  41 => 8,  35 => 7,  32 => 4,  27 => 6,);
    }
}

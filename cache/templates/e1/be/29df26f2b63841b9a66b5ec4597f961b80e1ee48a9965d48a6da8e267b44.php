<?php

/* blocks/help_formation.twig */
class __TwigTemplate_e1be29df26f2b63841b9a66b5ec4597f961b80e1ee48a9965d48a6da8e267b44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("help.twig");

        $this->blocks = array(
            'help_content' => array($this, 'block_help_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "help.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_help_content($context, array $blocks = array())
    {
        // line 4
        echo $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "help_formation"), "method");
        echo "
";
    }

    public function getTemplateName()
    {
        return "blocks/help_formation.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,);
    }
}

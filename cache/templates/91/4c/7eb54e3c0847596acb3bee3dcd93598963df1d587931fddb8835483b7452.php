<?php

/* blocks/finances_summary.twig */
class __TwigTemplate_914c7eb54e3c0847596acb3bee3dcd93598963df1d587931fddb8835483b7452 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["majorPositions"]) ? $context["majorPositions"] : null)) > 0)) {
            // line 2
            echo "<table class=\"table table-striped\">
\t<tbody>
\t\t";
            // line 4
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["majorPositions"]) ? $context["majorPositions"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["majorPosition"]) {
                // line 5
                echo "\t\t\t<tr>
\t\t\t\t<td><strong>
\t\t\t\t\t";
                // line 7
                if ($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "hasMessage", array(0 => $this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "subject")), "method")) {
                    // line 8
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => $this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "subject")), "method"), "html", null, true);
                    echo "
\t\t\t\t\t";
                } else {
                    // line 10
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "subject"), "html", null, true);
                    echo "
\t\t\t\t\t";
                }
                // line 11
                echo "\t\t
\t\t\t\t</strong></td>
\t\t\t\t<td>
\t\t\t\t\t";
                // line 14
                if (($this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "balance") < 0)) {
                    echo "<span style=\"color: red\">";
                }
                // line 15
                echo "\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "balance"), 0, ",", " "), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
                echo "
\t\t\t\t\t";
                // line 16
                if (($this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "balance") < 0)) {
                    echo "</span>";
                }
                echo "<br/>
\t\t\t\t\t(&empty; ";
                // line 17
                if (($this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "avgAmount") < 0)) {
                    echo "<span style=\"color: red\">";
                }
                // line 18
                echo "\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "avgAmount"), 0, ",", " "), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
                echo "
\t\t\t\t\t";
                // line 19
                if (($this->getAttribute((isset($context["majorPosition"]) ? $context["majorPosition"] : null), "avgAmount") < 0)) {
                    echo "</span>";
                }
                echo ")\t\t
\t\t\t\t</td>
\t\t\t</tr>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['majorPosition'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "\t</tbody>
</table>
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/finances_summary.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 23,  80 => 19,  56 => 15,  52 => 14,  47 => 11,  29 => 5,  25 => 4,  19 => 1,  123 => 25,  107 => 22,  105 => 21,  95 => 18,  73 => 18,  69 => 17,  64 => 11,  60 => 10,  41 => 10,  39 => 5,  35 => 8,  33 => 7,  21 => 2,  158 => 52,  153 => 49,  145 => 46,  141 => 45,  134 => 44,  130 => 43,  126 => 41,  120 => 39,  114 => 37,  112 => 23,  108 => 34,  102 => 20,  96 => 30,  94 => 29,  89 => 27,  86 => 26,  82 => 16,  75 => 21,  71 => 20,  67 => 12,  63 => 16,  57 => 9,  55 => 13,  46 => 7,  43 => 10,  40 => 9,  34 => 6,  31 => 5,  26 => 3,);
    }
}

<?php

/* blocks/facebookloginbox.twig */
class __TwigTemplate_a7ccd4ba0f70facef0a203e8ceb8fea09a957f127607cd3550d80825ba8f1847 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<p>
\t<a href=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["loginurl"]) ? $context["loginurl"] : null), "html", null, true);
        echo "\" class=\"btn btn-facebook\"><i class=\"icon-facebook-sign\"></i> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "facebook_login_button"), "method"), "html", null, true);
        echo "</a>
</p>";
    }

    public function getTemplateName()
    {
        return "blocks/facebookloginbox.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  607 => 127,  603 => 125,  592 => 124,  578 => 119,  571 => 117,  543 => 115,  538 => 114,  533 => 113,  529 => 112,  522 => 110,  516 => 107,  512 => 106,  508 => 104,  494 => 103,  481 => 100,  475 => 97,  464 => 94,  452 => 93,  437 => 88,  431 => 85,  426 => 82,  414 => 81,  402 => 78,  395 => 77,  389 => 76,  379 => 75,  376 => 74,  370 => 72,  364 => 70,  362 => 69,  358 => 68,  351 => 67,  347 => 65,  343 => 63,  341 => 62,  338 => 61,  321 => 60,  295 => 52,  285 => 49,  278 => 48,  263 => 47,  250 => 43,  243 => 42,  237 => 41,  221 => 40,  217 => 38,  211 => 36,  205 => 34,  203 => 33,  199 => 32,  192 => 31,  188 => 29,  184 => 27,  182 => 26,  179 => 25,  162 => 24,  149 => 20,  140 => 19,  134 => 18,  105 => 17,  101 => 15,  95 => 13,  89 => 11,  83 => 9,  72 => 6,  68 => 4,  63 => 2,  45 => 1,  37 => 123,  28 => 80,  25 => 59,  22 => 46,  19 => 1,  145 => 48,  136 => 47,  132 => 46,  129 => 45,  121 => 44,  118 => 43,  115 => 42,  112 => 41,  109 => 40,  107 => 39,  104 => 38,  102 => 37,  94 => 32,  87 => 10,  80 => 24,  76 => 8,  71 => 20,  66 => 3,  61 => 17,  56 => 15,  51 => 14,  49 => 13,  43 => 9,  40 => 131,  34 => 102,  31 => 92,  26 => 2,);
    }
}

<?php

/* blocks/alltimetable-table.twig */
class __TwigTemplate_97ed23a3a9b6a798edced9f4ef3183e5b89e46b2a5edecf72f28b9b0d75b8767 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("blocks/table_base.twig");

        $this->blocks = array(
            'table_header' => array($this, 'block_table_header'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "blocks/table_base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_table_header($context, array $blocks = array())
    {
        // line 5
        echo "\t<div class=\"btn-group\" style=\"margin-bottom: 10px;\">
\t\t<a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" href=\"#showAll\">
\t\t\t";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "leaguetable_button_type"), "method"), "html", null, true);
        echo "
\t\t\t<span class=\"caret\"></span>
\t\t</a>
\t\t<ul class=\"dropdown-menu\">
\t\t\t<li";
        // line 11
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "type"), "method") == null)) {
            echo " class=\"active\"";
        }
        echo "><a href=\"#wholeTable\" class=\"ajaxLink\"
\t\t\t\tdata-ajaxtarget=\"alltimetable-table_block\" data-ajaxblock=\"alltimetable-table\" data-ajaxquerystr=\"id=";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["leagueId"]) ? $context["leagueId"] : null), "html", null, true);
        echo "\"><i class=\"icon-list-alt\"></i> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "leaguetable_button_type_total"), "method"), "html", null, true);
        echo "</a></li>
\t\t\t<li";
        // line 13
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "type"), "method") == "home")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"#homeTable\" class=\"ajaxLink\"
\t\t\t\tdata-ajaxtarget=\"alltimetable-table_block\" data-ajaxblock=\"alltimetable-table\" data-ajaxquerystr=\"id=";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["leagueId"]) ? $context["leagueId"] : null), "html", null, true);
        echo "&type=home\"><i class=\"icon-arrow-right\"></i> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "leaguetable_button_type_home"), "method"), "html", null, true);
        echo "</a></li>
\t\t\t<li";
        // line 15
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "type"), "method") == "guest")) {
            echo " class=\"active\"";
        }
        echo "><a href=\"#guestTable\"class=\"ajaxLink\"
\t\t\t\tdata-ajaxtarget=\"alltimetable-table_block\" data-ajaxblock=\"alltimetable-table\" data-ajaxquerystr=\"id=";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["leagueId"]) ? $context["leagueId"] : null), "html", null, true);
        echo "&type=guest\"><i class=\"icon-arrow-left\"></i> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "leaguetable_button_type_guest"), "method"), "html", null, true);
        echo "</a></li>
\t\t</ul>
\t  
\t\t
\t</div>

\t<p><i class=\"icon-info-sign\"></i> <em>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "leaguetable_timedelay_info"), "method"), "html", null, true);
        echo "</em></p>
";
    }

    public function getTemplateName()
    {
        return "blocks/alltimetable-table.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 22,  72 => 16,  66 => 15,  60 => 14,  54 => 13,  48 => 12,  42 => 11,  35 => 7,  31 => 5,  28 => 4,  78 => 20,  73 => 17,  56 => 15,  52 => 14,  46 => 11,  41 => 8,  38 => 7,  32 => 4,  29 => 3,);
    }
}

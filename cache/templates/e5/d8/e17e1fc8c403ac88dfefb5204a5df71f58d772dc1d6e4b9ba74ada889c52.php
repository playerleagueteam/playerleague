<?php

/* layout.twig */
class __TwigTemplate_e5d8e17e1fc8c403ac88dfefb5204a5df71f58d772dc1d6e4b9ba74ada889c52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'browser_title' => array($this, 'block_browser_title'),
            'navigation' => array($this, 'block_navigation'),
            'page_header' => array($this, 'block_page_header'),
            'page_columns' => array($this, 'block_page_columns'),
            'messages_block' => array($this, 'block_messages_block'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<title>";
        // line 4
        $this->displayBlock('browser_title', $context, $blocks);
        echo "</title>

\t\t<meta charset=\"utf-8\" />
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />

\t\t";
        // line 9
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method") != null)) {
            // line 10
            echo "\t\t\t";
            $context["url"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => null, 1 => ("id=" . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method")), 2 => true), "method");
            // line 11
            echo "\t\t";
        } else {
            // line 12
            echo "\t\t\t";
            $context["url"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => null, 1 => null, 2 => true), "method");
            // line 13
            echo "\t\t";
        }
        // line 14
        echo "\t\t<link rel=\"canonical\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["url"]) ? $context["url"] : null), "html", null, true);
        echo "\"/>

\t\t";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["skin"]) ? $context["skin"] : null), "getCssSources", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["cssSource"]) {
            // line 17
            echo "\t\t<link href=\"";
            echo twig_escape_filter($this->env, (isset($context["cssSource"]) ? $context["cssSource"] : null), "html", null, true);
            echo "\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cssSource'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "\t\t";
        if (array_key_exists("cssReferences", $context)) {
            // line 20
            echo "\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cssReferences"]) ? $context["cssReferences"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["cssSource"]) {
                // line 21
                echo "\t\t\t<link href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
                echo "/css/";
                echo twig_escape_filter($this->env, (isset($context["cssSource"]) ? $context["cssSource"] : null), "html", null, true);
                echo "\" media=\"all\" rel=\"stylesheet\" type=\"text/css\" />
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cssSource'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "\t\t";
        }
        // line 24
        echo "
\t\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
        echo "/favicon.ico\" />

\t\t";
        // line 27
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "facebook_appid"), "method")) {
            // line 28
            echo "\t\t<meta property=\"fb:app_id\" content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "facebook_appid"), "method"), "html", null, true);
            echo "\"/>
\t\t";
        }
        // line 30
        echo "\t\t";
        echo $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "head_code"), "method");
        echo "
\t</head>
\t<body>

\t\t";
        // line 34
        $this->displayBlock('navigation', $context, $blocks);
        // line 37
        echo "
\t\t<div class=\"container container-pr\">

\t\t\t";
        // line 40
        $this->displayBlock('page_header', $context, $blocks);
        // line 42
        echo "
\t\t\t";
        // line 43
        $this->displayBlock('page_columns', $context, $blocks);
        // line 95
        echo "
\t\t\t<hr/>

\t\t\t";
        // line 98
        $this->displayBlock('footer', $context, $blocks);
        // line 101
        echo "


\t\t</div>

\t\t<!--[if lt IE 9]>
\t\t<script src=\"js/extcanvas/excanvas.compiled.js\"></script>
\t\t<![endif]-->
\t</body>
</html>
";
    }

    // line 4
    public function block_browser_title($context, array $blocks = array())
    {
        $this->displayBlock("page_title", $context, $blocks);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "projectname"), "method"), "html", null, true);
    }

    // line 34
    public function block_navigation($context, array $blocks = array())
    {
        // line 35
        echo "\t\t";
        $this->env->loadTemplate("navigationbar.twig")->display($context);
        // line 36
        echo "\t\t";
    }

    // line 40
    public function block_page_header($context, array $blocks = array())
    {
        // line 41
        echo "\t\t\t";
    }

    // line 43
    public function block_page_columns($context, array $blocks = array())
    {
        // line 44
        echo "\t\t\t<div class=\"col-md-12\">
\t\t\t\t<div class=\"col-md-8\" id=\"contentArea\">
\t\t\t\t\t<div class=\"\">

\t\t\t\t\t\t";
        // line 48
        $this->env->loadTemplate("breadcrumb.twig")->display($context);
        // line 49
        echo "
\t\t\t\t\t\t<div class=\"page-header\">
\t\t\t\t\t\t<h1>";
        // line 51
        $this->displayBlock("page_title", $context, $blocks);
        echo "</h1>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div id=\"messages\">
\t\t\t\t\t\t";
        // line 55
        $this->displayBlock('messages_block', $context, $blocks);
        // line 58
        echo "\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div id=\"pagecontent\">
\t\t\t\t\t\t";
        // line 61
        if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_top", array(), "any", true, true)) {
            // line 62
            echo "\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_top"));
            foreach ($context['_seq'] as $context["_key"] => $context["uiBlock"]) {
                // line 63
                echo "\t\t\t\t\t\t\t\t<div id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), "html", null, true);
                echo "_block\">
\t\t\t\t\t\t\t\t";
                // line 64
                echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), 1 => (isset($context["uiBlock"]) ? $context["uiBlock"] : null)), "method");
                echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['uiBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "\t\t\t\t\t\t";
        }
        // line 68
        echo "

\t\t\t\t\t\t";
        // line 70
        $this->displayBlock("page_content", $context, $blocks);
        echo "

\t\t\t\t\t\t";
        // line 72
        if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_bottom", array(), "any", true, true)) {
            // line 73
            echo "\t\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_bottom"));
            foreach ($context['_seq'] as $context["_key"] => $context["uiBlock"]) {
                // line 74
                echo "\t\t\t\t\t\t\t\t<div id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), "html", null, true);
                echo "_block\">
\t\t\t\t\t\t\t\t";
                // line 75
                echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), 1 => (isset($context["uiBlock"]) ? $context["uiBlock"] : null)), "method");
                echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['uiBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "\t\t\t\t\t\t";
        }
        // line 79
        echo "\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-4\">

\t\t\t\t\t";
        // line 85
        if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "sidebar", array(), "any", true, true)) {
            // line 86
            echo "\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "sidebar"));
            foreach ($context['_seq'] as $context["_key"] => $context["uiBlock"]) {
                // line 87
                echo "\t\t\t\t\t\t\t<div id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), "html", null, true);
                echo "_block\">
\t\t\t\t\t\t\t";
                // line 88
                echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), 1 => (isset($context["uiBlock"]) ? $context["uiBlock"] : null)), "method");
                echo "
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['uiBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 91
            echo "\t\t\t\t\t";
        }
        // line 92
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
    }

    // line 55
    public function block_messages_block($context, array $blocks = array())
    {
        // line 56
        echo "\t\t\t\t\t\t";
        echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => "messagesblock"), "method");
        echo "
\t\t\t\t\t\t";
    }

    // line 98
    public function block_footer($context, array $blocks = array())
    {
        // line 99
        echo "\t\t\t";
        $this->env->loadTemplate("footer.twig")->display($context);
        // line 100
        echo "\t\t\t";
    }

    public function getTemplateName()
    {
        return "layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 63,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 43,  173 => 41,  170 => 40,  163 => 35,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 21,  74 => 20,  71 => 19,  62 => 17,  58 => 16,  52 => 14,  46 => 12,  40 => 10,  38 => 9,  30 => 4,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 3,  34 => 2,  21 => 1,  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 49,  183 => 72,  180 => 44,  171 => 69,  166 => 36,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 28,  98 => 34,  96 => 25,  93 => 24,  86 => 27,  78 => 25,  75 => 24,  72 => 23,  69 => 22,  66 => 21,  64 => 20,  59 => 10,  55 => 17,  49 => 13,  47 => 12,  44 => 11,  41 => 4,  35 => 7,  32 => 6,  27 => 3,);
    }
}

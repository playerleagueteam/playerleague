<?php

/* navigationbar.twig */
class __TwigTemplate_aa4f65e2659fa8ff1ad7ae6455ac098ccbb79417d8d2170f2c6cefd6aeedc3a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["navitemTemplate"] = $this->env->loadTemplate("macros/navitem.twig");
        // line 2
        echo "
<div class=\"navbar navbar-default navbar-fixed-top\">
\t<div class=\"container\">
\t\t<div class=\"navbar-header\">
\t      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#websoccer-nav\" aria-expanded=\"false\">
\t        <span class=\"sr-only\">Toggle navigation</span>
\t        <span class=\"icon-bar\"></span>
\t        <span class=\"icon-bar\"></span>
\t        <span class=\"icon-bar\"></span>
\t      </button>
\t      <a class=\"navbar-brand\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getNavigationLabel", array(0 => "home"), "method"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "projectname"), "method"), "html", null, true);
        echo "</a>
\t    </div>
\t\t
\t\t<div class=\"collapse navbar-collapse\" id=\"websoccer-nav\">
\t\t\t<ul class=\"nav navbar-nav\" role=\"menu\">
\t\t\t\t";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["navItems"]) ? $context["navItems"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["navItem"]) {
            // line 18
            echo "\t\t\t\t
\t\t\t\t";
            // line 19
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "children")) > 0)) {
                // line 20
                echo "\t\t\t\t<li role=\"menuitem\" class=\"dropdown";
                if ($this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "isActive")) {
                    echo "  active";
                }
                echo "\">
\t\t\t\t\t<a class=\"dropdown-toggle\" id=\"label";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "pageId"), "html", null, true);
                echo "\" role=\"button\"
\t\t\t\t\t\tdata-toggle=\"dropdown\" href=\"#\">
\t\t\t\t\t\t";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "label"), "html", null, true);
                echo " <b class=\"caret\"></b>
\t\t\t\t\t</a>
\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"label";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "pageId"), "html", null, true);
                echo "\">
\t\t\t\t\t\t";
                // line 26
                echo $context["navitemTemplate"]->getitem($this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "pageId"), $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "label"), false);
                echo "
\t\t\t\t\t\t";
                // line 27
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "children"));
                foreach ($context['_seq'] as $context["_key"] => $context["navChild"]) {
                    // line 28
                    echo "\t\t\t\t\t\t";
                    echo $context["navitemTemplate"]->getitem($this->getAttribute((isset($context["navChild"]) ? $context["navChild"] : null), "pageId"), $this->getAttribute((isset($context["navChild"]) ? $context["navChild"] : null), "label"), $this->getAttribute((isset($context["navChild"]) ? $context["navChild"] : null), "isActive"));
                    echo "
\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['navChild'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "\t\t\t\t\t</ul>
\t\t\t\t</li>
\t\t\t\t";
            } else {
                // line 33
                echo "\t\t\t\t";
                echo $context["navitemTemplate"]->getitem($this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "pageId"), $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "label"), $this->getAttribute((isset($context["navItem"]) ? $context["navItem"] : null), "isActive"));
                echo "
\t\t\t\t";
            }
            // line 35
            echo "
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['navItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t\t\t</ul>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "navigationbar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 37,  97 => 33,  92 => 30,  83 => 28,  61 => 21,  54 => 20,  45 => 17,  33 => 12,  19 => 1,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 63,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 43,  173 => 41,  170 => 40,  163 => 35,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 27,  74 => 20,  71 => 25,  62 => 17,  58 => 16,  52 => 19,  46 => 12,  40 => 10,  38 => 9,  30 => 4,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 3,  34 => 2,  21 => 2,  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 49,  183 => 72,  180 => 44,  171 => 69,  166 => 36,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 34,  96 => 25,  93 => 24,  86 => 27,  78 => 25,  75 => 26,  72 => 23,  69 => 22,  66 => 23,  64 => 20,  59 => 10,  55 => 17,  49 => 18,  47 => 12,  44 => 11,  41 => 4,  35 => 7,  32 => 6,  27 => 3,);
    }
}

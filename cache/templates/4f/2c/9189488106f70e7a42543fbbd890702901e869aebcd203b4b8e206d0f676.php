<?php

/* breadcrumb.twig */
class __TwigTemplate_4f2c9189488106f70e7a42543fbbd890702901e869aebcd203b4b8e206d0f676 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (array_key_exists("breadcrumbItems", $context)) {
            // line 2
            echo "<ul class=\"breadcrumb\">
\t";
            // line 3
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbItems"]) ? $context["breadcrumbItems"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["breadcrumbItemKey"] => $context["breadcrumbItemLabel"]) {
                // line 4
                echo "\t\t";
                if ($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) {
                    // line 5
                    echo "\t\t<li class=\"active\">";
                    echo twig_escape_filter($this->env, (isset($context["breadcrumbItemLabel"]) ? $context["breadcrumbItemLabel"] : null), "html", null, true);
                    echo "</li>
\t\t";
                } else {
                    // line 7
                    echo "\t\t<li>
\t\t\t<a href=\"";
                    // line 8
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => (isset($context["breadcrumbItemKey"]) ? $context["breadcrumbItemKey"] : null)), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["breadcrumbItemLabel"]) ? $context["breadcrumbItemLabel"] : null), "html", null, true);
                    echo "</a>
\t\t</li>
\t\t";
                }
                // line 11
                echo "\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['breadcrumbItemKey'], $context['breadcrumbItemLabel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "breadcrumb.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 12,  61 => 11,  53 => 8,  24 => 3,  21 => 2,  19 => 1,  279 => 84,  273 => 82,  268 => 79,  249 => 73,  242 => 71,  238 => 69,  230 => 67,  222 => 65,  220 => 64,  217 => 63,  215 => 62,  209 => 59,  205 => 58,  198 => 56,  194 => 55,  190 => 53,  179 => 52,  175 => 51,  171 => 50,  163 => 47,  155 => 45,  147 => 43,  145 => 42,  141 => 40,  137 => 39,  132 => 36,  126 => 34,  124 => 33,  119 => 31,  115 => 30,  107 => 25,  90 => 23,  85 => 20,  82 => 19,  79 => 18,  76 => 17,  73 => 16,  70 => 15,  67 => 14,  50 => 7,  46 => 11,  44 => 5,  41 => 4,  38 => 8,  32 => 5,  29 => 4,);
    }
}

<?php

/* footer.twig */
class __TwigTemplate_c674546577e128feaca504ef7db9ecb9e2ea77d0ac3a44cd5c63d5e07902c71e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-md-12\">
  <div class=\"col-md-4\">
    <strong>Link Utili</strong>
    <ul>
      <li>ciao</li>
      <li>ciao</li>
      <li>ciao</li>
      <li>ciao</li>
    </ul>
  </div>
  <div class=\"col-md-4\">
    <strong>Link Utili</strong>
    <ul>
      <li>ciao</li>
      <li>ciao</li>
      <li>ciao</li>
      <li>ciao</li>
    </ul>
  </div>
  <div class=\"col-md-4\">
    <strong>Link Utili</strong>
    <ul>
      <li>ciao</li>
      <li>ciao</li>
      <li>ciao</li>
      <li>ciao</li>
    </ul>
  </div>
</div>

<footer>
      <p><a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "termsandconditions"), "method"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "termsandconditions_navlabel"), "method"), "html", null, true);
        echo "</a>
      ";
        // line 33
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "privacypolicy_url"), "method")) > 0)) {
            echo "| <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "privacypolicy_url"), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "privacypolicy_navlabel"), "method"), "html", null, true);
            echo "</a>";
        }
        // line 34
        echo "      | <a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "imprint"), "method"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "imprint_navlabel"), "method"), "html", null, true);
        echo "</a>
      | <i class=\"icon-time\"></i> ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDatetime", array(0 => null), "method"), "html", null, true);
        echo "
      </p>
</footer>


<div id=\"ajaxLoaderPage\"></div>

";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["skin"]) ? $context["skin"] : null), "getJavaScriptSources", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["jsSource"]) {
            // line 43
            echo "<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["jsSource"]) ? $context["jsSource"] : null), "html", null, true);
            echo "\" ></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['jsSource'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        if (array_key_exists("scriptReferences", $context)) {
            // line 46
            echo "  ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["scriptReferences"]) ? $context["scriptReferences"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["jsSource"]) {
                // line 47
                echo "  <script src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
                echo "/js/";
                echo twig_escape_filter($this->env, (isset($context["jsSource"]) ? $context["jsSource"] : null), "html", null, true);
                echo "\" ></script>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['jsSource'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 43,  73 => 35,  57 => 24,  102 => 19,  63 => 12,  50 => 9,  24 => 3,  268 => 86,  262 => 85,  256 => 81,  247 => 79,  242 => 78,  240 => 77,  237 => 76,  228 => 74,  224 => 73,  221 => 72,  203 => 68,  189 => 61,  168 => 53,  159 => 49,  155 => 47,  153 => 46,  149 => 45,  146 => 44,  144 => 43,  137 => 41,  133 => 40,  113 => 33,  100 => 30,  88 => 27,  85 => 26,  77 => 23,  70 => 21,  51 => 12,  42 => 5,  26 => 3,  22 => 2,  67 => 20,  29 => 3,  31 => 5,  23 => 3,  110 => 32,  97 => 33,  92 => 28,  83 => 42,  61 => 21,  54 => 23,  45 => 10,  33 => 10,  19 => 1,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 69,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 57,  173 => 56,  170 => 40,  163 => 51,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 16,  74 => 20,  71 => 25,  62 => 13,  58 => 33,  52 => 32,  46 => 7,  40 => 10,  38 => 3,  30 => 9,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 2,  34 => 6,  21 => 2,  215 => 85,  210 => 82,  200 => 67,  195 => 77,  190 => 76,  188 => 49,  183 => 60,  180 => 44,  171 => 69,  166 => 52,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 47,  98 => 46,  96 => 45,  93 => 17,  86 => 27,  78 => 25,  75 => 26,  72 => 22,  69 => 22,  66 => 34,  64 => 19,  59 => 10,  55 => 17,  49 => 21,  47 => 11,  44 => 11,  41 => 8,  35 => 11,  32 => 8,  27 => 4,);
    }
}

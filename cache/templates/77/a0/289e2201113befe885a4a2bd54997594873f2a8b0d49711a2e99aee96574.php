<?php

/* views/myschedule.twig */
class __TwigTemplate_77a0289e2201113befe885a4a2bd54997594873f2a8b0d49711a2e99aee96574 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_page_title($context, array $blocks = array())
    {
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "myschedule_navlabel"), "method"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_page_content($context, array $blocks = array())
    {
        // line 9
        echo "
";
        // line 10
        $this->env->loadTemplate("blocks/results-list.twig")->display($context);
        // line 11
        echo "
";
    }

    public function getTemplateName()
    {
        return "views/myschedule.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 11,  44 => 10,  41 => 9,  38 => 8,  32 => 5,  29 => 4,);
    }
}

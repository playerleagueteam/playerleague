<?php

/* base.twig */
class __TwigTemplate_2ec7c697f9008acc4a9ac02a8159d9345c9278e08170e29b8697d1e992f648e1 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        return $this->env->resolveTemplate((((array_key_exists("ajaxRequest", $context) && (isset($context["ajaxRequest"]) ? $context["ajaxRequest"] : null))) ? ("ajax.twig") : ("layout.twig")));
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 75,  183 => 72,  180 => 71,  171 => 69,  166 => 68,  164 => 67,  145 => 51,  139 => 47,  129 => 44,  124 => 43,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 34,  96 => 33,  93 => 32,  86 => 27,  78 => 25,  75 => 24,  72 => 23,  69 => 22,  66 => 21,  64 => 20,  59 => 18,  55 => 17,  49 => 13,  47 => 12,  44 => 11,  41 => 10,  35 => 7,  32 => 6,  27 => 3,);
    }
}

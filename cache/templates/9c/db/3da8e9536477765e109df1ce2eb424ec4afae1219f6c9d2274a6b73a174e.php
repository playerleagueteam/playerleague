<?php

/* blocks/facebook_comments.twig */
class __TwigTemplate_9cdb3da8e9536477765e109df1ce2eb424ec4afae1219f6c9d2274a6b73a174e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "facebook_enable_comments"), "method")) {
            // line 2
            echo "\t";
            $context["url"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => null, 1 => ("id=" . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method")), 2 => true), "method");
            // line 3
            echo "
\t<hr>
\t<div id=\"disqus_thread\"></div>
\t<script>
\t/**
\t* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
\t* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
\t*/
\t/*
\tvar disqus_config = function () {
\tthis.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
\tthis.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
\t};
\t*/
\t(function() { // DON'T EDIT BELOW THIS LINE
\tvar d = document, s = d.createElement('script');

\ts.src = '//playerleague.disqus.com/embed.js';

\ts.setAttribute('data-timestamp', +new Date());
\t(d.head || d.body).appendChild(s);
\t})();
\t</script>
\t<noscript>Please enable JavaScript to view the <a href=\"https://disqus.com/?ref_noscript\" rel=\"nofollow\">comments powered by Disqus.</a></noscript>
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/facebook_comments.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 3,  19 => 1,  46 => 5,  38 => 4,  34 => 2,  21 => 2,  544 => 211,  540 => 209,  534 => 207,  532 => 206,  522 => 198,  517 => 195,  511 => 194,  508 => 193,  505 => 192,  502 => 191,  500 => 190,  493 => 187,  487 => 185,  485 => 184,  482 => 183,  476 => 181,  469 => 180,  462 => 179,  460 => 178,  456 => 176,  452 => 174,  448 => 172,  444 => 170,  440 => 168,  436 => 166,  432 => 164,  428 => 162,  424 => 160,  420 => 158,  416 => 156,  414 => 155,  410 => 153,  407 => 152,  405 => 151,  400 => 149,  396 => 147,  390 => 143,  386 => 141,  380 => 138,  376 => 136,  373 => 135,  370 => 133,  365 => 132,  363 => 131,  360 => 130,  354 => 127,  350 => 125,  348 => 124,  339 => 117,  333 => 115,  331 => 114,  321 => 111,  313 => 110,  300 => 104,  296 => 103,  286 => 100,  282 => 99,  276 => 96,  271 => 93,  267 => 91,  259 => 89,  257 => 88,  252 => 86,  247 => 84,  244 => 83,  242 => 82,  237 => 79,  231 => 77,  228 => 76,  222 => 74,  220 => 73,  213 => 68,  196 => 66,  192 => 65,  188 => 63,  171 => 61,  167 => 60,  162 => 57,  154 => 56,  150 => 55,  144 => 53,  136 => 52,  128 => 46,  122 => 45,  119 => 44,  116 => 43,  113 => 42,  110 => 41,  107 => 40,  101 => 39,  99 => 38,  93 => 35,  84 => 29,  77 => 24,  72 => 22,  69 => 21,  67 => 20,  64 => 19,  60 => 17,  56 => 15,  52 => 13,  50 => 12,  47 => 11,  44 => 10,  36 => 7,  33 => 6,  28 => 4,  26 => 3,);
    }
}

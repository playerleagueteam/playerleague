<?php

/* blocks/ads_top.twig */
class __TwigTemplate_7c7e9afac0e9d5d2bdd66c85f41ba0be07bc30869479a1bb72d3175222c3432b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["adcode"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "frontend_ads_code_top"), "method");
        // line 2
        if (twig_length_filter($this->env, (isset($context["adcode"]) ? $context["adcode"] : null))) {
            // line 3
            echo "\t";
            echo (isset($context["adcode"]) ? $context["adcode"] : null);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/ads_top.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  110 => 37,  97 => 33,  92 => 30,  83 => 28,  61 => 21,  54 => 20,  45 => 17,  33 => 12,  19 => 1,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 63,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 43,  173 => 41,  170 => 40,  163 => 35,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 27,  74 => 20,  71 => 25,  62 => 17,  58 => 16,  52 => 19,  46 => 12,  40 => 10,  38 => 9,  30 => 4,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 3,  34 => 2,  21 => 2,  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 49,  183 => 72,  180 => 44,  171 => 69,  166 => 36,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 34,  96 => 25,  93 => 24,  86 => 27,  78 => 25,  75 => 26,  72 => 23,  69 => 22,  66 => 23,  64 => 20,  59 => 10,  55 => 17,  49 => 18,  47 => 12,  44 => 11,  41 => 4,  35 => 7,  32 => 6,  27 => 3,);
    }
}

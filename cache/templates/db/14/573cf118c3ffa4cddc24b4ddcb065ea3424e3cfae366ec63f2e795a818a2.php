<?php

/* blocks/season-picker.twig */
class __TwigTemplate_db14573cf118c3ffa4cddc24b4ddcb065ea3424e3cfae366ec63f2e795a818a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>";
        echo twig_escape_filter($this->env, (isset($context["league_name"]) ? $context["league_name"] : null), "html", null, true);
        echo "</h3>

<form class=\"form-horizontal\">
\t<div class=\"control-group\">
  \t\t<label class=\"control-label\" for=\"seasonid\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_season"), "method"), "html", null, true);
        echo "</label>
  \t\t<div class=\"controls\">
\t  \t\t<select name=\"seasonid\" id=\"seasonid\" required>
\t  \t\t\t<option></option>
\t  \t\t\t";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["seasons"]) ? $context["seasons"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["season"]) {
            // line 10
            echo "\t  \t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["season"]) ? $context["season"] : null), "id"), "html", null, true);
            echo "\"";
            if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "seasonid"), "method") == $this->getAttribute((isset($context["season"]) ? $context["season"] : null), "id"))) {
                echo " selected";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["season"]) ? $context["season"] : null), "name"), "html", null, true);
            echo "</option>
\t  \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['season'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "\t  \t\t</select>
  \t\t</div>
  \t</div>
  \t
  \t";
        // line 16
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "seasonid"), "method") > 0)) {
            // line 17
            echo "\t<div class=\"control-group\">
  \t\t<label class=\"control-label\" for=\"matchday\">";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_match_spieltag"), "method"), "html", null, true);
            echo "</label>
  \t\t<div class=\"controls\">
\t  \t\t<input id=\"matchday\" name=\"matchday\" type=\"number\" value=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method"), "html", null, true);
            echo "\" required/>
  \t\t</div>
  \t</div>
  \t";
        }
        // line 24
        echo "  \t
  \t<div class=\"control-group\">
  \t\t<div class=\"controls\">
\t  \t\t<button type=\"submit\" class=\"btn btn-primary ajaxSubmit\" 
\t\tdata-ajaxtarget=\"season-picker_block\" data-ajaxblock=\"season-picker\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_display"), "method"), "html", null, true);
        echo "</button>
\t  \t
\t  \t\t";
        // line 30
        if (((isset($context["currentMatchDay"]) ? $context["currentMatchDay"] : null) > 0)) {
            // line 31
            echo "\t  \t\t\t<a href=\"#\" class=\"btn ajaxLink\"
\tdata-ajaxtarget=\"season-picker_block\" data-ajaxblock=\"season-picker\"
\tdata-ajaxquerystr=\"leagueid=";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "leagueid"), "method"), "html", null, true);
            echo "&seasonid=";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "seasonid"), "method"), "html", null, true);
            echo "&matchday=";
            echo twig_escape_filter($this->env, (isset($context["currentMatchDay"]) ? $context["currentMatchDay"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "seasonpicker_currentmachday"), "method"), "html", null, true);
            echo "</a>
\t  \t\t";
        }
        // line 35
        echo "\t  \t
\t  \t</div>
  \t</div>
  \t<input type=\"hidden\" name=\"page\" value=\"results\" />
  \t<input type=\"hidden\" name=\"leagueid\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "leagueid"), "method"), "html", null, true);
        echo "\" />
</form>

<ul class=\"pager\">
\t";
        // line 43
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method") > 1)) {
            // line 44
            echo "  <li class=\"previous\">
    <a href=\"#\" class=\"ajaxLink\"
\tdata-ajaxtarget=\"season-picker_block\" data-ajaxblock=\"season-picker\"
\tdata-ajaxquerystr=\"leagueid=";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "leagueid"), "method"), "html", null, true);
            echo "&seasonid=";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "seasonid"), "method"), "html", null, true);
            echo "&matchday=";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method") - 1), "html", null, true);
            echo "\">&larr; ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "seasonpicker_previous"), "method"), "html", null, true);
            echo "</a>
  </li>
  ";
        }
        // line 50
        echo "
  ";
        // line 51
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method") < (isset($context["maxMatchDay"]) ? $context["maxMatchDay"] : null))) {
            // line 52
            echo "  <li class=\"next\">
    <a href=\"#\" class=\"ajaxLink\"
\tdata-ajaxtarget=\"season-picker_block\" data-ajaxblock=\"season-picker\"
\tdata-ajaxquerystr=\"leagueid=";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "leagueid"), "method"), "html", null, true);
            echo "&seasonid=";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "seasonid"), "method"), "html", null, true);
            echo "&matchday=";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method") + 1), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "seasonpicker_next"), "method"), "html", null, true);
            echo " &rarr;</a>
  </li>
  ";
        }
        // line 58
        echo "</ul>

";
        // line 60
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method") > 0)) {
            // line 61
            echo "\t<h4>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_match_spieltag"), "method"), "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method"), "html", null, true);
            echo "</h4>
\t<p><a href=\"";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "teamoftheday", 1 => ((((("leagueid=" . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "leagueid"), "method")) . "&seasonid=") . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "seasonid"), "method")) . "&matchday=") . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "matchday"), "method"))), "method"), "html", null, true);
            echo "\" class=\"btn btn-small\"><i class=\"icon-heart-empty\"></i> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "teamoftheday_navlabel"), "method"), "html", null, true);
            echo "</a></p>
";
        }
        // line 64
        echo "<span id=\"results-list_block\">";
        echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => "results-list"), "method");
        echo "</span>";
    }

    public function getTemplateName()
    {
        return "blocks/season-picker.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 64,  171 => 62,  164 => 61,  162 => 60,  158 => 58,  146 => 55,  141 => 52,  139 => 51,  136 => 50,  124 => 47,  119 => 44,  117 => 43,  110 => 39,  104 => 35,  93 => 33,  89 => 31,  87 => 30,  82 => 28,  76 => 24,  69 => 20,  64 => 18,  61 => 17,  59 => 16,  53 => 12,  38 => 10,  34 => 9,  27 => 5,  19 => 1,);
    }
}

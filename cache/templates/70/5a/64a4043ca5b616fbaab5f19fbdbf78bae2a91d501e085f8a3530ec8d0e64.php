<?php

/* macros/profileelements.twig */
class __TwigTemplate_705a64a4043ca5b616fbaab5f19fbdbf78bae2a91d501e085f8a3530ec8d0e64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
    }

    // line 1
    public function getinfofield($_label = null, $_value = null, $_doNotEscape = false)
    {
        $context = $this->env->mergeGlobals(array(
            "label" => $_label,
            "value" => $_value,
            "doNotEscape" => $_doNotEscape,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
<div class=\"row-fluid inforow\">
\t<div class=\"span6 infolabel\">";
            // line 4
            if ((isset($context["doNotEscape"]) ? $context["doNotEscape"] : null)) {
                echo (isset($context["label"]) ? $context["label"] : null);
            } else {
                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
            }
            echo "</div>
\t<div class=\"span6 infovalue\">";
            // line 5
            if ((isset($context["doNotEscape"]) ? $context["doNotEscape"] : null)) {
                echo (isset($context["value"]) ? $context["value"] : null);
            } else {
                echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
            }
            echo "</div>
</div>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "macros/profileelements.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 5,  38 => 4,  34 => 2,  21 => 1,  544 => 211,  540 => 209,  534 => 207,  532 => 206,  522 => 198,  517 => 195,  511 => 194,  508 => 193,  505 => 192,  502 => 191,  500 => 190,  493 => 187,  487 => 185,  485 => 184,  482 => 183,  476 => 181,  469 => 180,  462 => 179,  460 => 178,  456 => 176,  452 => 174,  448 => 172,  444 => 170,  440 => 168,  436 => 166,  432 => 164,  428 => 162,  424 => 160,  420 => 158,  416 => 156,  414 => 155,  410 => 153,  407 => 152,  405 => 151,  400 => 149,  396 => 147,  390 => 143,  386 => 141,  380 => 138,  376 => 136,  373 => 135,  370 => 133,  365 => 132,  363 => 131,  360 => 130,  354 => 127,  350 => 125,  348 => 124,  339 => 117,  333 => 115,  331 => 114,  321 => 111,  313 => 110,  300 => 104,  296 => 103,  286 => 100,  282 => 99,  276 => 96,  271 => 93,  267 => 91,  259 => 89,  257 => 88,  252 => 86,  247 => 84,  244 => 83,  242 => 82,  237 => 79,  231 => 77,  228 => 76,  222 => 74,  220 => 73,  213 => 68,  196 => 66,  192 => 65,  188 => 63,  171 => 61,  167 => 60,  162 => 57,  154 => 56,  150 => 55,  144 => 53,  136 => 52,  128 => 46,  122 => 45,  119 => 44,  116 => 43,  113 => 42,  110 => 41,  107 => 40,  101 => 39,  99 => 38,  93 => 35,  84 => 29,  77 => 24,  72 => 22,  69 => 21,  67 => 20,  64 => 19,  60 => 17,  56 => 15,  52 => 13,  50 => 12,  47 => 11,  44 => 10,  36 => 7,  33 => 6,  28 => 4,  26 => 3,);
    }
}

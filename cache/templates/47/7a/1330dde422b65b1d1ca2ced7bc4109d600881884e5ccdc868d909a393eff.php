<?php

/* views/home.twig */
class __TwigTemplate_477a1330dde422b65b1d1ca2ced7bc4109d600881884e5ccdc868d909a393eff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'browser_title' => array($this, 'block_browser_title'),
            'page_header' => array($this, 'block_page_header'),
            'page_columns' => array($this, 'block_page_columns'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["msgbox"] = $this->env->loadTemplate("macros/messagebox.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_browser_title($context, array $blocks = array())
    {
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "home_title", 1 => $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "projectname"), "method")), "method"), "html", null, true);
        echo "
";
    }

    // line 10
    public function block_page_header($context, array $blocks = array())
    {
        // line 11
        echo "
";
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "id") == null)) {
            // line 13
            echo "<div class=\"row\">
\t<div class=\"col-md-12\">
\t\t<div class=\"jumbotron\">
\t\t\t<div class=\"container\">
\t\t\t  <h1>";
            // line 17
            $this->displayBlock("browser_title", $context, $blocks);
            echo "</h1>
\t\t\t  <p>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "home_teaser"), "method"), "html", null, true);
            echo "</p>

\t\t\t  \t";
            // line 20
            if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "allow_userregistration"), "method") || (twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method")) > 0))) {
                // line 21
                echo "\t\t\t\t\t";
                $context["registerUrl"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "register"), "method");
                // line 22
                echo "\t\t\t\t\t";
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method")) > 0)) {
                    // line 23
                    echo "\t\t\t\t\t\t";
                    $context["registerUrl"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method");
                    // line 24
                    echo "\t\t\t\t\t";
                }
                // line 25
                echo "\t\t\t\t\t<div style=\"text-align:center;\"><a href=\"";
                echo twig_escape_filter($this->env, (isset($context["registerUrl"]) ? $context["registerUrl"] : null), "html", null, true);
                echo "\" class=\"btn btn-primary btn-lg\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "home_teaser_link_register"), "method"), "html", null, true);
                echo "</a></div>
\t\t\t\t";
            }
            // line 27
            echo "\t\t\t</div>
\t  \t</div>
\t</div>
</div>
";
        }
        // line 32
        echo "
";
        // line 33
        if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_top", array(), "any", true, true)) {
            // line 34
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_top"));
            foreach ($context['_seq'] as $context["_key"] => $context["uiBlock"]) {
                // line 35
                echo "\t\t<div id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), "html", null, true);
                echo "_block\">
\t\t";
                // line 36
                echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), 1 => (isset($context["uiBlock"]) ? $context["uiBlock"] : null)), "method");
                echo "
\t\t</div>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['uiBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 40
        echo "
<div id=\"messages\">
";
        // line 42
        if (array_key_exists("frontMessages", $context)) {
            // line 43
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["frontMessages"]) ? $context["frontMessages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 44
                echo "\t\t";
                echo $context["msgbox"]->getbox($this->getAttribute((isset($context["message"]) ? $context["message"] : null), "title"), $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "message"), $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "type"));
                echo "
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 47
        echo "</div>

<div class=\"col-md-12\">
\t<div class=\"col-md-3\">
\t\t";
        // line 51
        echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => "news"), "method");
        echo "

\t\t<div class=\"ajaxLoadedBlock\" id=\"projectStatisticsBlock\"
\t\t\tdata-ajaxblock=\"projectstatistics\" data-ignoreemptymessages=\"true\">
\t\t</div>
\t</div>
\t<div class=\"col-md-6\">

\t\t<div class=\"ajaxLoadedBlock\" id=\"latestResultsBlock\"
\t\t\t\tdata-ajaxblock=\"latest-results-summary\" data-ignoreemptymessages=\"true\"></div>

\t\t\t<div class=\"ajaxLoadedBlock\" id=\"latestActivitiesBlock\"
\t\t\t\tdata-ajaxblock=\"user-activities-all\" data-ignoreemptymessages=\"true\" data-refreshperiod=\"10\"></div>
\t</div>

\t<div class=\"col-md-3\">
\t\t";
        // line 67
        if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "sidebar", array(), "any", true, true)) {
            // line 68
            echo "\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "sidebar"));
            foreach ($context['_seq'] as $context["_key"] => $context["uiBlock"]) {
                // line 69
                echo "\t\t\t\t";
                echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), 1 => (isset($context["uiBlock"]) ? $context["uiBlock"] : null)), "method");
                echo "
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['uiBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "\t\t";
        }
        // line 72
        echo "\t</div>
</div>

";
        // line 75
        if ($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_bottom", array(), "any", true, true)) {
            // line 76
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["blocks"]) ? $context["blocks"] : null), "content_bottom"));
            foreach ($context['_seq'] as $context["_key"] => $context["uiBlock"]) {
                // line 77
                echo "\t\t<div id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), "html", null, true);
                echo "_block\">
\t\t";
                // line 78
                echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => $this->getAttribute((isset($context["uiBlock"]) ? $context["uiBlock"] : null), "id"), 1 => (isset($context["uiBlock"]) ? $context["uiBlock"] : null)), "method");
                echo "
\t\t</div>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['uiBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 82
        echo "
";
    }

    // line 85
    public function block_page_columns($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "views/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 75,  183 => 72,  180 => 71,  171 => 69,  166 => 68,  164 => 67,  145 => 51,  139 => 47,  129 => 44,  124 => 43,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 34,  96 => 33,  93 => 32,  86 => 27,  78 => 25,  75 => 24,  72 => 23,  69 => 22,  66 => 21,  64 => 20,  59 => 18,  55 => 17,  49 => 13,  47 => 12,  44 => 11,  41 => 10,  35 => 7,  32 => 6,  27 => 3,);
    }
}

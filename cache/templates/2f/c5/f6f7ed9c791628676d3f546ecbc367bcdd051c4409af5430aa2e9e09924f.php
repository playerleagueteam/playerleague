<?php

/* blocks/userprofile.twig */
class __TwigTemplate_2fc5f6f7ed9c791628676d3f546ecbc367bcdd051c4409af5430aa2e9e09924f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("box.twig");

        $this->blocks = array(
            'box_title' => array($this, 'block_box_title'),
            'box_content' => array($this, 'block_box_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "box.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["statisticelements"] = $this->env->loadTemplate("macros/statisticelements.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_box_title($context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"btn-group\" style=\"float:right\"> 
 <!--";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_link_logout"), "method"), "html", null, true);
        echo " -->
<a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalActionUrl", array(0 => "logout"), "method"), "html", null, true);
        echo "\">
<button type=\"button\" class=\"btn btn-default\"><i class=\"icon-off\"></i> </button></a>
<!-- ";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_link_profile"), "method"), "html", null, true);
        echo "</a> -->
<a href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "profile"), "method"), "html", null, true);
        echo "\">
<button type=\"button\" class=\"btn btn-default\"><i class=\"icon-edit\"></i>  </button></a>
</div>

";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_title"), "method"), "html", null, true);
        echo "
";
    }

    // line 19
    public function block_box_content($context, array $blocks = array())
    {
        // line 20
        echo "
";
        // line 21
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "profilepicture")) > 0)) {
            // line 22
            echo "\t<a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "user", 1 => ("id=" . $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "id"))), "method"), "html", null, true);
            echo "\">
\t\t<img src=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "profilepicture"), "html", null, true);
            echo "\" class=\"pull-left\" style=\"width: 40px; height: 40px; margin-right: 10px\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "username"), "html", null, true);
            echo "\"/>
\t</a>
";
        }
        // line 26
        echo "
<p><em>";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_loggedin_as"), "method"), "html", null, true);
        echo "</em><br/>
<a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "user", 1 => ("id=" . $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "id"))), "method"), "html", null, true);
        echo "\"><strong>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "username"), "html", null, true);
        echo "</strong></a>
";
        // line 29
        if (((isset($context["unseenMessages"]) ? $context["unseenMessages"] : null) > 0)) {
            // line 30
            echo "(<a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "messages"), "method"), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "messages_new_messages", 1 => (isset($context["unseenMessages"]) ? $context["unseenMessages"] : null)), "method"), "html", null, true);
            echo "\"><i class=\"icon-envelope\"></i><small>";
            echo twig_escape_filter($this->env, (isset($context["unseenMessages"]) ? $context["unseenMessages"] : null), "html", null, true);
            echo "</small></a>)
";
        }
        // line 32
        echo "
<a id=\"notificationsLink\" href=\"#\" title=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_notifications"), "method"), "html", null, true);
        echo "\"
\tclass=\"ajaxLink\" data-ajaxtarget=\"notificationspopup\" data-ajaxblock=\"notifications\"><span class=\"badge";
        // line 34
        if (((isset($context["unseenNotifications"]) ? $context["unseenNotifications"] : null) > 0)) {
            echo " badge-important";
        }
        echo "\"><i class=\"icon-bell icon-white\"></i>";
        if (((isset($context["unseenNotifications"]) ? $context["unseenNotifications"] : null) > 0)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["unseenNotifications"]) ? $context["unseenNotifications"] : null), "html", null, true);
        }
        echo "</span></a>
</p>
<div id=\"notificationspopupwrapper\" style=\"display: none\"><div id=\"notificationspopup\">...</div></div>


<!-- 
<p><em>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_highscore"), "method"), "html", null, true);
        echo "</em><br/>
<a href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "highscore"), "method"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "user_highscore"), 0, ",", " "), "html", null, true);
        echo "</a></p> -->

";
        // line 43
        if (($this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getUser", array(), "method"), "clubId") > 0)) {
            // line 44
            echo "
<p> <em>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_club"), "method"), "html", null, true);
            echo "</em>
";
            // line 46
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_picture"))) {
                // line 47
                echo "\t";
                $context["teamPicture"] = (((("<img src=\"" . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method")) . "/uploads/club/") . $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_picture")) . "\" style=\"width: 20px; height: 20px;\" />");
            } else {
                // line 49
                echo "\t";
                $context["teamPicture"] = "";
            }
            // line 51
            echo "
";
            // line 52
            if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "max_number_teams_per_user"), "method") > 1)) {
                // line 53
                echo "</p>

<div class=\"dropdown\" style=\"margin-top: -11px; margin-bottom: 12px;\">
  ";
                // line 56
                echo (isset($context["teamPicture"]) ? $context["teamPicture"] : null);
                echo " <a class=\"dropdown-toggle ajaxLink\" data-toggle=\"dropdown\" href=\"#\" data-ajaxblock=\"user-clubs-selection\" data-ajaxtarget=\"clubsSelectionList\">
    ";
                // line 57
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_name"), "html", null, true);
                echo "
    <b class=\"caret\"></b>
  </a>
  <a href=\"";
                // line 60
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_id"))), "method"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "team_navlabel"), "method"), "html", null, true);
                echo "\" style=\"margin: 0 5px\"><i class=\"icon-info-sign darkIcon\"></i></a> 
  <a href=\"";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "league", 1 => ("id=" . $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_league_id"))), "method"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_link_table"), "method"), "html", null, true);
                echo "\"><i class=\"icon-table darkIcon\"></i></a>
  <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dLabel\" id=\"clubsSelectionList\">
    
  </ul>
</div>
";
            } else {
                // line 67
                echo "\t<br>
\t";
                // line 68
                echo (isset($context["teamPicture"]) ? $context["teamPicture"] : null);
                echo " <a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_id"))), "method"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "team_navlabel"), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_name"), "html", null, true);
                echo "</a> 
\t<a href=\"";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "league", 1 => ("id=" . $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_league_id"))), "method"), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_link_table"), "method"), "html", null, true);
                echo "\"><i class=\"icon-table darkIcon\"></i></a>
\t</p>
";
            }
            // line 72
            echo "
<p><em>";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_club_finanz_budget"), "method"), "html", null, true);
            echo "</em><br/>
<a href=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "finances"), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["userteam"]) ? $context["userteam"] : null), "team_budget"), 0, ",", " "), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
            echo "</a></p>
";
        }
        // line 76
        echo "
";
        // line 77
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "premium_enabled"), "method")) {
            // line 78
            echo "\t<p><em>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_premium_balance"), "method"), "html", null, true);
            echo "</em><br/>
\t<a href=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "premiumaccount"), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "premiumBalance"), 0, ",", " "), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "premium_credit_unit"), "method"), "html", null, true);
            echo "</a></p>
";
        }
        // line 81
        echo "
\t<div class=\"ajaxLoadedBlock\" id=\"nextmatchBlock\" data-ajaxblock=\"user-next-match\" data-ignoreemptymessages=\"true\" data-refreshperiod=\"100\"></div> 


<p><em>";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_popularity"), "method"), "html", null, true);
        echo "</em> <i class=\"icon-question-sign wstooltip\" data-toggle=\"tooltip\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "userprofile_block_tooltip_popularity"), "method"), "html", null, true);
        echo "\"></i></p>
";
        // line 86
        echo $context["statisticelements"]->getprogressbar($this->getAttribute((isset($context["profile"]) ? $context["profile"] : null), "user_popularity"));
        echo "

";
    }

    public function getTemplateName()
    {
        return "blocks/userprofile.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 86,  262 => 85,  256 => 81,  247 => 79,  242 => 78,  240 => 77,  237 => 76,  228 => 74,  224 => 73,  221 => 72,  203 => 68,  189 => 61,  168 => 53,  159 => 49,  155 => 47,  153 => 46,  149 => 45,  146 => 44,  144 => 43,  137 => 41,  133 => 40,  113 => 33,  100 => 30,  88 => 27,  85 => 26,  77 => 23,  70 => 21,  51 => 12,  42 => 9,  26 => 3,  22 => 2,  67 => 20,  29 => 3,  31 => 5,  23 => 4,  110 => 32,  97 => 33,  92 => 28,  83 => 28,  61 => 21,  54 => 20,  45 => 10,  33 => 12,  19 => 1,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 69,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 57,  173 => 56,  170 => 40,  163 => 51,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 27,  74 => 20,  71 => 25,  62 => 13,  58 => 16,  52 => 19,  46 => 12,  40 => 10,  38 => 8,  30 => 4,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 3,  34 => 6,  21 => 3,  215 => 85,  210 => 82,  200 => 67,  195 => 77,  190 => 76,  188 => 49,  183 => 60,  180 => 44,  171 => 69,  166 => 52,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 29,  96 => 25,  93 => 24,  86 => 27,  78 => 25,  75 => 26,  72 => 22,  69 => 22,  66 => 23,  64 => 19,  59 => 10,  55 => 17,  49 => 11,  47 => 11,  44 => 11,  41 => 8,  35 => 7,  32 => 4,  27 => 6,);
    }
}

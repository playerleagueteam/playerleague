<?php

/* blocks/socialrecommendations.twig */
class __TwigTemplate_6a8dd573fe68a2703c16154339d4383563f26eb8856dfd517081e74e686ceced extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "social_likebutton_werkenntwen"), "method")) {
            // line 2
            echo "<script type=\"text/javascript\" id=\"wkwrecommendscript\" src=\"http://www.wer-kennt-wen.de/js/widgets/external/recommend.js?imgId=0\"></script>
<style>
#wkwRecommendAnchorTag > img {
\tvertical-align: top;
}
</style>
";
        }
        // line 9
        echo "
";
        // line 10
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "social_likebutton_xing"), "method")) {
            // line 11
            echo "<div data-counter=\"no_count\" data-type=\"XING/Share\" data-lang=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getCurrentLanguage", array(), "method"), "html", null, true);
            echo "\"></div>
<script>
  ;(function (d, s) {
    var x = d.createElement(s),
      s = d.getElementsByTagName(s)[0];
      x.src = \"https://www.xing-share.com/js/external/share.js\";
      s.parentNode.insertBefore(x, s);
  })(document, \"script\");
</script>
";
        }
        // line 21
        echo "
";
        // line 22
        if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "social_likebutton_twitter"), "method")) {
            // line 23
            echo "<span style=\"margin: 0 2px;\">
<a href=\"https://twitter.com/share\" class=\"twitter-share-button\" data-lang=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getCurrentLanguage", array(), "method"), "html", null, true);
            echo "\">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=\"https://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>
</span>";
        }
    }

    public function getTemplateName()
    {
        return "blocks/socialrecommendations.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 24,  102 => 19,  63 => 12,  50 => 9,  24 => 3,  268 => 86,  262 => 85,  256 => 81,  247 => 79,  242 => 78,  240 => 77,  237 => 76,  228 => 74,  224 => 73,  221 => 72,  203 => 68,  189 => 61,  168 => 53,  159 => 49,  155 => 47,  153 => 46,  149 => 45,  146 => 44,  144 => 43,  137 => 41,  133 => 40,  113 => 33,  100 => 30,  88 => 27,  85 => 26,  77 => 23,  70 => 21,  51 => 12,  42 => 5,  26 => 3,  22 => 2,  67 => 20,  29 => 3,  31 => 5,  23 => 3,  110 => 32,  97 => 33,  92 => 28,  83 => 28,  61 => 21,  54 => 23,  45 => 10,  33 => 10,  19 => 1,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 69,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 57,  173 => 56,  170 => 40,  163 => 51,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 16,  74 => 20,  71 => 25,  62 => 13,  58 => 16,  52 => 22,  46 => 7,  40 => 10,  38 => 3,  30 => 9,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 2,  34 => 6,  21 => 2,  215 => 85,  210 => 82,  200 => 67,  195 => 77,  190 => 76,  188 => 49,  183 => 60,  180 => 44,  171 => 69,  166 => 52,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 29,  96 => 18,  93 => 17,  86 => 27,  78 => 25,  75 => 26,  72 => 22,  69 => 22,  66 => 23,  64 => 19,  59 => 10,  55 => 17,  49 => 21,  47 => 11,  44 => 11,  41 => 8,  35 => 11,  32 => 8,  27 => 4,);
    }
}

<?php

/* blocks/transfers-list.twig */
class __TwigTemplate_0634908c508330ef379e902c5ffa0a9253aa699b17796202f3ccdd7ca25d955b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((array_key_exists("completedtransfers", $context) && (twig_length_filter($this->env, (isset($context["completedtransfers"]) ? $context["completedtransfers"] : null)) > 0))) {
            // line 2
            echo "
\t<table class=\"table table-striped\">
\t\t<thead>
\t\t\t<tr>
\t\t\t\t<th>";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "transfer_date"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_player"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "transfer_from"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "transfer_to"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "transfer_bid_amount"), "method"), "html", null, true);
            echo "</th>
\t\t\t</tr>
\t\t</thead>
\t\t<tbody>
  
\t  \t";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["completedtransfers"]) ? $context["completedtransfers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["transfer"]) {
                // line 16
                echo "\t\t\t<tr>
\t\t\t\t<td>";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDate", array(0 => $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "transfer_date")), "method"), "html", null, true);
                echo "</td>
\t\t\t\t<td><a href=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "player", 1 => ("id=" . $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "player_id"))), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "player_firstname"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "player_lastname"), "html", null, true);
                echo "</a></td>
\t\t\t\t<td>";
                // line 19
                if (($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "from_id") > 0)) {
                    // line 20
                    echo "\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "from_id"))), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "from_name"), "html", null, true);
                    echo "</a>
\t\t\t\t";
                } else {
                    // line 21
                    echo "-";
                }
                // line 22
                echo "\t\t\t\t</td>
\t\t\t\t<td><a href=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "to_id"))), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "to_name"), "html", null, true);
                echo "</a></td>
\t\t\t\t<td>
\t\t\t\t\t";
                // line 25
                if (($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "hand_money") > 0)) {
                    // line 26
                    echo "\t\t\t\t\t\t<p><small>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "transfer_bid_handmoney"), "method"), "html", null, true);
                    echo "</small><br>
\t\t\t\t\t\t";
                    // line 27
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "hand_money"), 0, ",", " "), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
                    echo "</p>
\t\t\t\t\t";
                }
                // line 29
                echo "\t\t\t\t
\t\t\t\t\t<p>
\t\t\t\t\t\t";
                // line 31
                if (($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "directtransfer_amount") > 0)) {
                    // line 32
                    echo "\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "directtransfer_amount"), 0, ",", " "), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
                    echo "
\t\t\t\t\t\t";
                } else {
                    // line 34
                    echo "\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "amount"), 0, ",", " "), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
                    echo "
\t\t\t\t\t\t";
                }
                // line 36
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 37
                if ((($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_id") > 0) || ($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_id") > 0))) {
                    // line 38
                    echo "\t\t\t\t\t\t\t<ul class=\"unstyled\">
\t\t\t\t\t\t\t\t";
                    // line 39
                    if (($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_id") > 0)) {
                        // line 40
                        echo "\t\t\t\t\t\t\t\t<li><small>+ <a href=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "player", 1 => ("id=" . $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_id"))), "method"), "html", null, true);
                        echo "\">";
                        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_pseudonym")) > 0)) {
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_pseudonym"), "html", null, true);
                        } else {
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_firstname"), "html", null, true);
                            echo " ";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer1_lastname"), "html", null, true);
                        }
                        echo "</a></small></li>
\t\t\t\t\t\t\t\t";
                    }
                    // line 42
                    echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                    // line 43
                    if (($this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_id") > 0)) {
                        // line 44
                        echo "\t\t\t\t\t\t\t\t<li><small>+ <a href=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "player", 1 => ("id=" . $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_id"))), "method"), "html", null, true);
                        echo "\">";
                        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_pseudonym")) > 0)) {
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_pseudonym"), "html", null, true);
                        } else {
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_firstname"), "html", null, true);
                            echo " ";
                            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transfer"]) ? $context["transfer"] : null), "exchangeplayer2_lastname"), "html", null, true);
                        }
                        echo "</a></small></li>
\t\t\t\t\t\t\t\t";
                    }
                    // line 46
                    echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t";
                }
                // line 48
                echo "\t\t\t\t\t
\t\t\t\t\t</p>
\t\t\t\t</td>
\t\t\t</tr>
\t  \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['transfer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "\t  \t
\t\t</tbody>
\t</table>


";
        } else {
            // line 59
            echo "\t<p>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "transfers_no_transfers"), "method"), "html", null, true);
            echo "</p>
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/transfers-list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 59,  186 => 53,  176 => 48,  172 => 46,  158 => 44,  156 => 43,  153 => 42,  139 => 40,  137 => 39,  134 => 38,  132 => 37,  129 => 36,  121 => 34,  111 => 31,  107 => 29,  100 => 27,  95 => 26,  93 => 25,  86 => 23,  83 => 22,  80 => 21,  72 => 20,  62 => 18,  51 => 15,  43 => 10,  39 => 9,  35 => 8,  27 => 6,  21 => 2,  19 => 1,  760 => 261,  757 => 260,  755 => 259,  749 => 257,  747 => 256,  744 => 255,  738 => 253,  732 => 251,  730 => 250,  723 => 245,  717 => 243,  710 => 241,  707 => 240,  667 => 238,  664 => 237,  656 => 235,  653 => 234,  651 => 233,  647 => 232,  640 => 228,  631 => 222,  620 => 216,  611 => 210,  607 => 209,  602 => 207,  598 => 206,  593 => 204,  589 => 203,  584 => 201,  580 => 200,  575 => 198,  571 => 197,  566 => 195,  562 => 194,  556 => 191,  548 => 186,  544 => 185,  539 => 183,  535 => 182,  530 => 180,  526 => 179,  521 => 177,  517 => 176,  512 => 174,  508 => 173,  503 => 171,  499 => 170,  493 => 167,  483 => 159,  478 => 157,  472 => 156,  467 => 154,  461 => 153,  456 => 151,  450 => 150,  445 => 148,  439 => 147,  434 => 145,  428 => 144,  422 => 142,  420 => 141,  417 => 140,  413 => 138,  405 => 136,  403 => 135,  399 => 134,  396 => 133,  390 => 130,  387 => 129,  383 => 127,  375 => 125,  373 => 124,  369 => 123,  366 => 122,  364 => 121,  353 => 115,  349 => 114,  346 => 113,  339 => 111,  334 => 110,  332 => 109,  325 => 107,  321 => 106,  314 => 104,  310 => 103,  303 => 101,  299 => 100,  286 => 92,  282 => 91,  275 => 89,  271 => 88,  264 => 86,  260 => 85,  253 => 83,  249 => 82,  233 => 80,  230 => 79,  227 => 78,  224 => 77,  221 => 76,  218 => 75,  216 => 74,  212 => 73,  209 => 72,  204 => 70,  199 => 69,  192 => 67,  187 => 66,  185 => 65,  181 => 63,  173 => 60,  167 => 58,  165 => 57,  161 => 55,  157 => 53,  154 => 52,  148 => 50,  146 => 49,  138 => 47,  136 => 46,  131 => 44,  128 => 43,  123 => 41,  118 => 40,  116 => 39,  113 => 32,  108 => 36,  103 => 35,  101 => 34,  96 => 32,  92 => 31,  78 => 20,  74 => 19,  70 => 19,  66 => 16,  58 => 17,  55 => 16,  53 => 11,  50 => 10,  47 => 9,  34 => 6,  31 => 7,  26 => 3,);
    }
}

<?php

/* views/strikers.twig */
class __TwigTemplate_552efd3fabd2f7c25889781719a35bc37184eb37489ebb72330d0650712f701f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["messagebox"] = $this->env->loadTemplate("macros/messagebox.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_page_title($context, array $blocks = array())
    {
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "topstrikers_title"), "method"), "html", null, true);
        echo "
";
    }

    // line 9
    public function block_page_content($context, array $blocks = array())
    {
        // line 10
        echo "
<form class=\"form-inline\" method=\"post\">

\t<label class=\"control-label\" for=\"country\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_league"), "method"), "html", null, true);
        echo "</label> 
\t<select id=\"leagueid\" name=\"leagueid\">
\t\t<option></option>
\t";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["leagues"]) ? $context["leagues"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["leagueItem"]) {
            // line 17
            echo "\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["leagueItem"]) ? $context["leagueItem"] : null), "league_id"), "html", null, true);
            echo "\"";
            if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "leagueid"), "method") == $this->getAttribute((isset($context["leagueItem"]) ? $context["leagueItem"] : null), "league_id"))) {
                echo " selected";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["leagueItem"]) ? $context["leagueItem"] : null), "league_country"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["leagueItem"]) ? $context["leagueItem"] : null), "league_name"), "html", null, true);
            echo "</option>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['leagueItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "\t</select>

\t<button type=\"submit\" class=\"btn btn-primary ajaxSubmit\" 
\t\tdata-ajaxtarget=\"pagecontent\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_display"), "method"), "html", null, true);
        echo "</button>

\t<input type=\"hidden\" name=\"page\" value=\"topstrikers\" />
</form>

";
        // line 27
        if ((array_key_exists("players", $context) && (twig_length_filter($this->env, (isset($context["players"]) ? $context["players"] : null)) > 0))) {
            // line 28
            echo "
\t<table class=\"table table-striped\">
\t\t<thead>
\t\t\t<tr>
\t\t\t\t<th>#</th>
\t\t\t\t<th>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "playertable_head_name"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_player_verein_id"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_player_sa_spiele"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_player_sa_tore"), "method"), "html", null, true);
            echo "</th>
\t\t\t</tr>
\t\t</thead>
\t\t<tbody>
\t\t
\t\t\t";
            // line 41
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["players"]) ? $context["players"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
                // line 42
                echo "\t\t\t<tr>
\t\t\t\t<td><strong>";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "</strong></td>
\t\t\t\t<td><p><a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "player", 1 => ("id=" . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"))), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "firstname"), "html", null, true);
                echo " ";
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym")) > 0)) {
                    echo "\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym"), "html", null, true);
                    echo "\" ";
                }
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "lastname"), "html", null, true);
                echo "</a></p></td>
\t\t\t\t<td>
\t\t\t\t";
                // line 46
                if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "team_id") > 0)) {
                    // line 47
                    echo "\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "team", 1 => ("id=" . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "team_id"))), "method"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "team_name"), "html", null, true);
                    echo "</a>
\t\t\t\t";
                } else {
                    // line 49
                    echo "\t\t\t\t-
\t\t\t\t";
                }
                // line 51
                echo "\t\t\t\t</td>
\t\t\t\t<td>";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "matches"), "html", null, true);
                echo "</td>
\t\t\t\t<td>";
                // line 53
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "goals"), "html", null, true);
                echo "</td>
\t\t\t</tr>
\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "\t\t
\t\t</tbody>
\t</table>

";
        } else {
            // line 61
            echo "\t";
            echo $context["messagebox"]->getbox($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "no_players_found"), "method"), "", "info");
            echo "
";
        }
        // line 63
        echo "
";
    }

    public function getTemplateName()
    {
        return "views/strikers.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 63,  200 => 61,  193 => 56,  176 => 53,  172 => 52,  169 => 51,  165 => 49,  157 => 47,  155 => 46,  141 => 44,  137 => 43,  134 => 42,  117 => 41,  109 => 36,  105 => 35,  101 => 34,  97 => 33,  90 => 28,  88 => 27,  80 => 22,  75 => 19,  58 => 17,  54 => 16,  48 => 13,  43 => 10,  40 => 9,  34 => 6,  31 => 5,  26 => 3,);
    }
}

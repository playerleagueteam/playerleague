<?php

/* box.twig */
class __TwigTemplate_f3da764eb384de487125b758d29c46fb45942df16c3423b2c98a5f0ab4f33449 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"box\">
\t<h4>";
        // line 2
        $this->displayBlock("box_title", $context, $blocks);
        echo "</h4>
\t";
        // line 3
        $this->displayBlock("box_content", $context, $blocks);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  67 => 16,  29 => 3,  31 => 8,  23 => 4,  110 => 37,  97 => 33,  92 => 30,  83 => 28,  61 => 21,  54 => 20,  45 => 10,  33 => 12,  19 => 1,  317 => 100,  314 => 99,  311 => 98,  304 => 56,  301 => 55,  295 => 92,  292 => 91,  283 => 88,  278 => 87,  273 => 86,  271 => 85,  263 => 79,  260 => 78,  251 => 75,  246 => 74,  241 => 73,  239 => 72,  234 => 70,  230 => 68,  227 => 67,  218 => 64,  213 => 63,  208 => 62,  206 => 61,  201 => 58,  199 => 55,  192 => 51,  186 => 48,  177 => 43,  173 => 41,  170 => 40,  163 => 35,  160 => 34,  152 => 4,  138 => 101,  136 => 98,  131 => 95,  126 => 42,  119 => 37,  117 => 34,  109 => 30,  101 => 27,  90 => 23,  79 => 27,  74 => 20,  71 => 25,  62 => 13,  58 => 16,  52 => 19,  46 => 12,  40 => 10,  38 => 7,  30 => 4,  25 => 1,  53 => 8,  48 => 6,  43 => 11,  36 => 3,  34 => 2,  21 => 3,  215 => 85,  210 => 82,  200 => 78,  195 => 77,  190 => 76,  188 => 49,  183 => 72,  180 => 44,  171 => 69,  166 => 36,  164 => 67,  145 => 51,  139 => 47,  129 => 43,  124 => 40,  122 => 42,  118 => 40,  108 => 36,  103 => 35,  98 => 34,  96 => 25,  93 => 24,  86 => 27,  78 => 25,  75 => 26,  72 => 23,  69 => 22,  66 => 23,  64 => 20,  59 => 10,  55 => 17,  49 => 11,  47 => 12,  44 => 11,  41 => 8,  35 => 7,  32 => 4,  27 => 6,);
    }
}

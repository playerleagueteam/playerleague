<?php

/* views/results-overview.twig */
class __TwigTemplate_38a0626e9778f3654184693c450c6e86027c7c9742f009a24887809e886d157e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_page_title($context, array $blocks = array())
    {
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "results_navlabel"), "method"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_page_content($context, array $blocks = array())
    {
        // line 8
        echo "
<ul class=\"nav nav-tabs\" id=\"resultsTab\">
  <li class=\"active\"><a href=\"#leagues\" data-toggle=\"tab\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "results_tab_leagues"), "method"), "html", null, true);
        echo "</a></li>
  <li><a href=\"#cups\" data-toggle=\"tab\" class=\"ajaxLink\"
  \tdata-ajaxblock=\"cups-list\" data-ajaxtarget=\"cups-content\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "results_tab_cups"), "method"), "html", null, true);
        echo "</a></li>
  <li><a href=\"#latest\" data-toggle=\"tab\" class=\"ajaxLink\"
  \tdata-ajaxblock=\"latest-results\" data-ajaxtarget=\"latest-results_block\">";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "results_tab_latest_results"), "method"), "html", null, true);
        echo "</a></li>
</ul>

<div class=\"tab-content\">
\t<div class=\"tab-pane active\" id=\"leagues\">
\t
\t\t<div class=\"row-fluid\">
\t\t\t<div class=\"span3\">

\t\t\t\t
\t\t\t\t";
        // line 24
        if ((array_key_exists("countries", $context) && (twig_length_filter($this->env, (isset($context["countries"]) ? $context["countries"] : null)) > 0))) {
            // line 25
            echo "\t\t\t\t<div class=\"accordion\" id=\"countries\">
\t\t\t\t\t";
            // line 26
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["countryName"] => $context["leagues"]) {
                // line 27
                echo "\t\t\t\t\t\t<div class=\"accordion-group\">
\t\t\t\t\t\t\t<div class=\"accordion-heading\">
\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#countries\" href=\"#collapse";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "\"><strong>";
                echo twig_escape_filter($this->env, (isset($context["countryName"]) ? $context["countryName"] : null), "html", null, true);
                echo "</strong></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div id=\"collapse";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "\" class=\"accordion-body collapse\">
\t\t\t\t\t\t\t\t<div class=\"accordion-inner\">
\t\t\t\t\t\t\t\t\t<ul class=\"unstyled\">
\t\t\t\t\t\t\t\t\t";
                // line 34
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["leagues"]) ? $context["leagues"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["league"]) {
                    // line 35
                    echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"#league";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["league"]) ? $context["league"] : null), "id"), "html", null, true);
                    echo "\" class=\"ajaxLink\"
\t\t\t\t\t\t\t\t\t\t\tdata-ajaxblock=\"season-picker\" data-ajaxtarget=\"season-picker_block\" 
\t\t\t\t\t\t\t\t\t\t\tdata-ajaxquerystr=\"leagueid=";
                    // line 37
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["league"]) ? $context["league"] : null), "id"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["league"]) ? $context["league"] : null), "name"), "html", null, true);
                    echo "</a></li>
\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['league'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 39
                echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['countryName'], $context['leagues'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "\t\t\t\t</div>
\t\t\t\t";
        }
        // line 46
        echo "\t\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t\t<div class=\"span9\">
\t\t\t
\t\t\t\t<span id=\"season-picker_block\">";
        // line 51
        echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => "season-picker"), "method");
        echo "</span>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t
\t</div>
\t
\t<div class=\"tab-pane\" id=\"cups\">
\t\t<div id=\"cups-content\"></div>
\t</div>
\t
\t<div class=\"tab-pane\" id=\"latest\">
\t\t<span id=\"latest-results_block\">";
        // line 63
        echo $this->getAttribute((isset($context["viewHandler"]) ? $context["viewHandler"] : null), "renderBlock", array(0 => "latest-results"), "method");
        echo "</span>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "views/results-overview.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 63,  156 => 51,  149 => 46,  145 => 44,  127 => 39,  117 => 37,  111 => 35,  107 => 34,  101 => 31,  94 => 29,  90 => 27,  73 => 26,  70 => 25,  68 => 24,  55 => 14,  50 => 12,  45 => 10,  41 => 8,  38 => 7,  32 => 4,  29 => 3,);
    }
}

<?php

/* views/finances.twig */
class __TwigTemplate_381ea90b979951facd1a119828a5b7061c71497137a2b5ebd6cfaa183cfa8579 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["pagination"] = $this->env->loadTemplate("macros/paginator.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_page_title($context, array $blocks = array())
    {
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "finances_navlabel"), "method"), "html", null, true);
        echo "
";
    }

    // line 9
    public function block_page_content($context, array $blocks = array())
    {
        // line 10
        echo "
<h3>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_club_finanz_budget"), "method"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["budget"]) ? $context["budget"] : null), 0, ",", " "), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
        echo "</h3>

";
        // line 13
        if ((array_key_exists("statements", $context) && (twig_length_filter($this->env, (isset($context["statements"]) ? $context["statements"] : null)) > 0))) {
            // line 14
            echo "
\t<table class=\"table table-striped\">
\t\t<thead>
\t\t\t<tr>
\t\t\t\t<th>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "account_statement_date"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "account_statement_sender"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "account_statement_subject"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "account_statement_amount"), "method"), "html", null, true);
            echo "</th>
\t\t\t</tr>
\t\t</thead>
\t\t<tbody>
\t\t";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["statements"]) ? $context["statements"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["statement"]) {
                // line 26
                echo "\t\t<tr>
\t\t\t<td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getFormattedDate", array(0 => $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "date")), "method"), "html", null, true);
                echo "</td>
\t\t\t<td>
\t\t\t\t";
                // line 29
                if ($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "hasMessage", array(0 => $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "sender")), "method")) {
                    // line 30
                    echo "\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "sender")), "method"), "html", null, true);
                    echo "
\t\t\t\t";
                } else {
                    // line 32
                    echo "\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "sender"), "html", null, true);
                    echo "
\t\t\t\t";
                }
                // line 34
                echo "\t\t\t</td>
\t\t\t<td>
\t\t\t\t";
                // line 36
                if ($this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "hasMessage", array(0 => $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "subject")), "method")) {
                    // line 37
                    echo "\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "subject")), "method"), "html", null, true);
                    echo "
\t\t\t\t";
                } else {
                    // line 39
                    echo "\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "subject"), "html", null, true);
                    echo "
\t\t\t\t";
                }
                // line 41
                echo "\t\t\t</td>
\t\t\t<td>
\t\t\t\t";
                // line 43
                if (($this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "amount") < 0)) {
                    echo "<span style=\"color: red\">";
                }
                // line 44
                echo "\t\t\t\t";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "amount"), 0, ",", " "), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "game_currency"), "method"), "html", null, true);
                echo "
\t\t\t\t";
                // line 45
                if (($this->getAttribute((isset($context["statement"]) ? $context["statement"] : null), "amount") < 0)) {
                    echo "</span>";
                }
                // line 46
                echo "\t\t\t</td>
\t\t</tr>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['statement'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 49
            echo "\t\t</tbody>
\t</table>
\t
\t";
            // line 52
            echo $context["pagination"]->getpaginator((isset($context["paginator"]) ? $context["paginator"] : null));
            echo "

";
        }
    }

    public function getTemplateName()
    {
        return "views/finances.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 52,  153 => 49,  145 => 46,  141 => 45,  134 => 44,  130 => 43,  126 => 41,  120 => 39,  114 => 37,  112 => 36,  108 => 34,  102 => 32,  96 => 30,  94 => 29,  89 => 27,  86 => 26,  82 => 25,  75 => 21,  71 => 20,  67 => 19,  63 => 18,  57 => 14,  55 => 13,  46 => 11,  43 => 10,  40 => 9,  34 => 6,  31 => 5,  26 => 3,);
    }
}

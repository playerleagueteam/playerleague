<?php

/* blocks/match_result_statistics.twig */
class __TwigTemplate_845f9ea2f0daed2101055947d752ed1ee6f58eb878ee1fcea2f21395995af884 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["statisticelements"] = $this->env->loadTemplate("macros/statisticelements.twig");
        // line 2
        echo "
<h3>";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_statistic_shoots"), "method"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "shoots"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "shoots"), "html", null, true);
        echo "</h3>

<div class=\"row-fluid\">
\t<div class=\"span6\">
\t<h3>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_statistic_tackles"), "method"), "html", null, true);
        echo "</h3>
\t";
        // line 8
        if ((($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "wontackles") > 0) || ($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "wontackles") > 0))) {
            // line 9
            echo $context["statisticelements"]->getpiechart($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name"), twig_number_format_filter($this->env, (($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "wontackles") * 100) / ($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "wontackles") + $this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "wontackles")))), $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name"), twig_number_format_filter($this->env, (($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "wontackles") * 100) / ($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "wontackles") + $this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "wontackles")))));
            echo "
\t";
        }
        // line 11
        echo "\t</div>
\t<div class=\"span6\">
\t<h3>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_statistic_ballcontacts"), "method"), "html", null, true);
        echo "</h3>
\t";
        // line 14
        if ((($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "ballcontacts") > 0) || ($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "ballcontacts") > 0))) {
            // line 15
            echo $context["statisticelements"]->getpiechart($this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name"), twig_number_format_filter($this->env, (($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "ballcontacts") * 100) / ($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "ballcontacts") + $this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "ballcontacts")))), $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name"), twig_number_format_filter($this->env, (($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "ballcontacts") * 100) / ($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "ballcontacts") + $this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "ballcontacts")))));
            echo "
\t";
        }
        // line 16
        echo "\t
\t\t</div>
\t</div>
\t
<h3>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_statistic_successfulpasses"), "method"), "html", null, true);
        echo "</h3>
  \t
";
        // line 22
        if ((($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "passes_successed") > 0) || ($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "passes_failed") > 0))) {
            // line 23
            $context["homePasses"] = twig_number_format_filter($this->env, (($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "passes_successed") * 100) / ($this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "passes_successed") + $this->getAttribute((isset($context["homeStatistics"]) ? $context["homeStatistics"] : null), "passes_failed"))));
            // line 24
            echo "
<p>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_home_name"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, (isset($context["homePasses"]) ? $context["homePasses"] : null), "html", null, true);
            echo "%)</p>
";
            // line 26
            echo $context["statisticelements"]->getprogressbar((isset($context["homePasses"]) ? $context["homePasses"] : null));
            echo "
";
        }
        // line 27
        echo " \t
 
";
        // line 29
        if ((($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "passes_successed") > 0) || ($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "passes_failed") > 0))) {
            // line 30
            $context["guestPasses"] = twig_number_format_filter($this->env, (($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "passes_successed") * 100) / ($this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "passes_successed") + $this->getAttribute((isset($context["guestStatistics"]) ? $context["guestStatistics"] : null), "passes_failed"))));
            // line 31
            echo "<p>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["match"]) ? $context["match"] : null), "match_guest_name"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, (isset($context["guestPasses"]) ? $context["guestPasses"] : null), "html", null, true);
            echo "%)</p>
";
            // line 32
            echo $context["statisticelements"]->getprogressbar((isset($context["guestPasses"]) ? $context["guestPasses"] : null));
            echo "
";
        }
        // line 33
        echo " ";
    }

    public function getTemplateName()
    {
        return "blocks/match_result_statistics.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 33,  105 => 32,  98 => 31,  96 => 30,  94 => 29,  90 => 27,  85 => 26,  79 => 25,  76 => 24,  74 => 23,  72 => 22,  67 => 20,  61 => 16,  56 => 15,  54 => 14,  50 => 13,  46 => 11,  41 => 9,  39 => 8,  35 => 7,  24 => 3,  21 => 2,  19 => 1,);
    }
}

<?php

/* blocks/userlogin.twig */
class __TwigTemplate_00606c3fdd1e6a05bcce75ff8cbc092f697c0ba5615da943030a0c048f3613e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("box.twig");

        $this->blocks = array(
            'box_title' => array($this, 'block_box_title'),
            'box_content' => array($this, 'block_box_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "box.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["formelements"] = $this->env->loadTemplate("macros/formelements.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_box_title($context, array $blocks = array())
    {
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formlogin_block_title"), "method"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_box_content($context, array $blocks = array())
    {
        // line 9
        echo "
<form method=\"post\">

\t<div class=\"form-group\">
\t";
        // line 13
        if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "login_type"), "method") == "email")) {
            // line 14
            echo "\t    <label for=\"loginstr\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_email"), "method"), "html", null, true);
            echo "</label>
\t    <input type=\"email\" class=\"form-control\" id=\"loginstr\" name=\"loginstr\" placeholder=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_email"), "method"), "html", null, true);
            echo "\" required>
 \t";
        } else {
            // line 17
            echo "\t \t<label for=\"loginstr\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_nick"), "method"), "html", null, true);
            echo "</label>
\t    <input type=\"text\" class=\"form-control\" id=\"loginstr\" name=\"loginstr\" placeholder=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_nick"), "method"), "html", null, true);
            echo "\" required>
 \t";
        }
        // line 20
        echo " \t</div>
 \t
 \t<div class=\"form-group\">
\t    <label for=\"loginpassword\">";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_passwort"), "method"), "html", null, true);
        echo "</label>
\t    <input type=\"password\" class=\"form-control\" id=\"loginpassword\" name=\"loginpassword\" placeholder=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_users_passwort"), "method"), "html", null, true);
        echo "\" required>
    </div>
    <div class=\"checkbox\">
\t    <label class=\"checkbox\">
\t      <input type=\"checkbox\" id=\"rememberme\" name=\"rememberme\" value=\"1\"> ";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formlogin_option_rememberme"), "method"), "html", null, true);
        echo "
\t    </label>
    </div>
    
\t<button type=\"submit\" class=\"btn btn-primary\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formlogin_submit_button"), "method"), "html", null, true);
        echo "</button>
\t<input type=\"hidden\" name=\"page\" value=\"login\"/>
\t<input type=\"hidden\" name=\"action\" value=\"login\"/>
</form>

\t";
        // line 37
        if ((($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "allow_userregistration"), "method") || $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "login_allow_sendingpassword"), "method")) || (twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method")) > 0))) {
            // line 38
            echo "\t\t<p>
\t\t\t";
            // line 39
            if (($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "allow_userregistration"), "method") || (twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method")) > 0))) {
                // line 40
                echo "\t\t\t\t";
                $context["registerUrl"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "register"), "method");
                // line 41
                echo "\t\t\t\t";
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method")) > 0)) {
                    // line 42
                    echo "\t\t\t\t\t";
                    $context["registerUrl"] = $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method");
                    // line 43
                    echo "\t\t\t\t";
                }
                // line 44
                echo "\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, (isset($context["registerUrl"]) ? $context["registerUrl"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formlogin_link_register"), "method"), "html", null, true);
                echo "</a>
\t\t\t";
            }
            // line 45
            echo " 
\t\t\t";
            // line 46
            if ((($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "allow_userregistration"), "method") || (twig_length_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "registration_url"), "method")) > 0)) && $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "login_allow_sendingpassword"), "method"))) {
                echo " | ";
            }
            // line 47
            echo "\t\t\t";
            if ($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "login_allow_sendingpassword"), "method")) {
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "forgot-password"), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formlogin_link_forgot_password"), "method"), "html", null, true);
                echo "</a>";
            }
            // line 48
            echo "\t\t</p>
\t";
        }
    }

    public function getTemplateName()
    {
        return "blocks/userlogin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 48,  136 => 47,  132 => 46,  129 => 45,  121 => 44,  118 => 43,  115 => 42,  112 => 41,  109 => 40,  107 => 39,  104 => 38,  102 => 37,  94 => 32,  87 => 28,  80 => 24,  76 => 23,  71 => 20,  66 => 18,  61 => 17,  56 => 15,  51 => 14,  49 => 13,  43 => 9,  40 => 8,  34 => 5,  31 => 4,  26 => 2,);
    }
}

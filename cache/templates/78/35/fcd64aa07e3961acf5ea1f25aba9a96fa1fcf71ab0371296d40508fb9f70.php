<?php

/* blocks/rss_results_link.twig */
class __TwigTemplate_7835fcd64aa07e3961acf5ea1f25aba9a96fa1fcf71ab0371296d40508fb9f70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p class=\"text-right\"><i class=\"icon-rss-sign\"></i> <a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "context_root"), "method"), "html", null, true);
        echo "/webservices/rss.php?page=rss-results&id=";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["env"]) ? $context["env"] : null), "user"), "id"), "html", null, true);
        echo "&lang=";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getCurrentLanguage", array(), "method"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "rss_link_label"), "method"), "html", null, true);
        echo "</a></p>";
    }

    public function getTemplateName()
    {
        return "blocks/rss_results_link.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  171 => 63,  156 => 51,  149 => 46,  145 => 44,  127 => 39,  117 => 37,  111 => 35,  107 => 34,  101 => 31,  94 => 29,  90 => 27,  73 => 26,  70 => 25,  68 => 24,  55 => 14,  50 => 12,  45 => 10,  41 => 8,  38 => 7,  32 => 4,  29 => 3,);
    }
}

<?php

/* views/match_live_changes.twig */
class __TwigTemplate_0d2b4a00ca4f7337ffef7d8e8698acc5b9bf8e896a8fb4e7cbb4f4dafd85c8ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.twig");

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["formelements"] = $this->env->loadTemplate("macros/formelements.twig");
        // line 3
        $context["statisticelements"] = $this->env->loadTemplate("macros/statisticelements.twig");
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_page_title($context, array $blocks = array())
    {
        // line 6
        echo "\t";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match-live-changes_navlabel"), "method"), "html", null, true);
        echo "
";
    }

    // line 9
    public function block_page_content($context, array $blocks = array())
    {
        // line 10
        echo "<form id=\"formationForm\" method=\"post\">
\t<h3>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_report_minute", 1 => (isset($context["minute"]) ? $context["minute"] : null)), "method"), "html", null, true);
        echo " (<a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "match", 1 => ("id=" . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method"))), "method"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_navlabel"), "method"), "html", null, true);
        echo "</a>)</h3>

\t<table class=\"table table-striped\">
\t\t<thead>
\t\t\t<tr>
\t\t\t\t<th>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_player"), "method"), "html", null, true);
        echo "</th>
\t\t\t\t<th>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "entity_player_position_main"), "method"), "html", null, true);
        echo "</th>
\t\t\t</tr>
\t\t</thead>
\t\t<tbody>
\t\t\t";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["players"]) ? $context["players"] : null), "field", array(), "array"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
            // line 22
            echo "\t\t\t\t";
            if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym", array(), "any", true, true) && (twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym")) > 0))) {
                // line 23
                echo "\t\t\t\t\t";
                $context["playerName"] = $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym");
                // line 24
                echo "\t\t\t\t";
            } else {
                // line 25
                echo "\t\t\t\t\t";
                $context["playerName"] = ((twig_slice($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "firstname"), 0, 1) . ". ") . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "lastname"));
                // line 26
                echo "\t\t\t\t";
            }
            // line 27
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td><a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "player", 1 => ("id=" . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"))), "method"), "html", null, true);
            echo "\" target=\"_blank\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "player_navlabel"), "method"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["playerName"]) ? $context["playerName"] : null), "html", null, true);
            echo "</a> (";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "grade"), "html", null, true);
            echo ")</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<input type=\"hidden\" name=\"player";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
            echo "\" class=\"playerField\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), ("player" . $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index")), array(), "array"), "html", null, true);
            echo "\">
\t\t\t\t\t\t";
            // line 31
            if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "match_position") == "Torwart")) {
                // line 32
                echo "\t\t\t\t\t\t\t<input type=\"hidden\" name=\"player";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "_pos\" value=\"T\">
\t\t\t\t\t\t\t";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "option_T"), "method"), "html", null, true);
                echo "
\t\t\t\t\t\t";
            } else {
                // line 35
                echo "\t\t\t\t\t\t\t";
                $context["playerPos"] = $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("player" . $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index")) . "_pos"), array(), "array");
                // line 36
                echo "\t\t\t\t\t\t\t<select name=\"player";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index"), "html", null, true);
                echo "_pos\">
\t\t\t\t\t\t\t\t<option value=\"\"></option>
\t\t\t\t\t\t\t\t";
                // line 38
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(array(0 => "LV", 1 => "IV", 2 => "RV", 3 => "DM", 4 => "LM", 5 => "ZM", 6 => "RM", 7 => "OM", 8 => "LS", 9 => "MS", 10 => "RS"));
                foreach ($context['_seq'] as $context["_key"] => $context["mainPos"]) {
                    // line 39
                    echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_escape_filter($this->env, (isset($context["mainPos"]) ? $context["mainPos"] : null), "html", null, true);
                    echo "\"";
                    if (((isset($context["playerPos"]) ? $context["playerPos"] : null) == (isset($context["mainPos"]) ? $context["mainPos"] : null))) {
                        echo " selected";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("option_" . (isset($context["mainPos"]) ? $context["mainPos"] : null))), "method"), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mainPos'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 41
                echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t";
            }
            // line 43
            echo "\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "\t\t</tbody>
\t</table>
\t
\t";
        // line 49
        if (($this->getAttribute((isset($context["players"]) ? $context["players"] : null), "bench", array(), "array", true, true) && (twig_length_filter($this->env, $this->getAttribute((isset($context["players"]) ? $context["players"] : null), "bench", array(), "array")) > 0))) {
            // line 50
            echo "\t<h4>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_substitutions_title"), "method"), "html", null, true);
            echo "</h4>
\t
\t<table class=\"table table-striped\">
\t\t<thead>
\t\t\t<tr>
\t\t\t\t<th>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_substitutions_out"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_substitutions_in"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_substitutions_minute"), "method"), "html", null, true);
            echo "</th>
\t\t\t\t<th>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_substitutions_condition"), "method"), "html", null, true);
            echo "</th>
\t\t\t</tr>
\t\t</thead>
\t\t<tbody>
\t\t\t";
            // line 62
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 3));
            foreach ($context['_seq'] as $context["_key"] => $context["subsNo"]) {
                // line 63
                echo "\t\t\t\t";
                if ((($this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_minute"), array(), "array") > 0) && ($this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_minute"), array(), "array") <= (isset($context["minute"]) ? $context["minute"] : null)))) {
                    // line 64
                    echo "\t\t\t\t\t";
                    $context["subIsDisabled"] = true;
                    // line 65
                    echo "\t\t\t\t";
                } else {
                    // line 66
                    echo "\t\t\t\t\t";
                    $context["subIsDisabled"] = false;
                    // line 67
                    echo "\t\t\t\t";
                }
                // line 68
                echo "\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<select name=\"sub";
                // line 70
                echo twig_escape_filter($this->env, (isset($context["subsNo"]) ? $context["subsNo"] : null), "html", null, true);
                echo "_out\" class=\"span2\"";
                if ((isset($context["subIsDisabled"]) ? $context["subIsDisabled"] : null)) {
                    echo " disabled";
                }
                echo ">
\t\t\t\t\t\t\t<option value=\"\"></option>
\t\t\t\t\t\t\t";
                // line 72
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["players"]) ? $context["players"] : null), "field", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
                    // line 73
                    echo "\t\t\t\t\t\t\t\t";
                    if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym", array(), "any", true, true) && (twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym")) > 0))) {
                        // line 74
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["playerName"] = $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym");
                        // line 75
                        echo "\t\t\t\t\t\t\t\t";
                    } else {
                        // line 76
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["playerName"] = ((twig_slice($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "firstname"), 0, 1) . ". ") . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "lastname"));
                        // line 77
                        echo "\t\t\t\t\t\t\t\t";
                    }
                    // line 78
                    echo "\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"), "html", null, true);
                    echo "\"";
                    if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id") == $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_out"), array(), "array"))) {
                        echo " selected";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, (isset($context["playerName"]) ? $context["playerName"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<select name=\"sub";
                // line 83
                echo twig_escape_filter($this->env, (isset($context["subsNo"]) ? $context["subsNo"] : null), "html", null, true);
                echo "_in\" class=\"span2\"";
                if ((isset($context["subIsDisabled"]) ? $context["subIsDisabled"] : null)) {
                    echo " disabled";
                }
                echo ">
\t\t\t\t\t\t\t<option value=\"\"></option>
\t\t\t\t\t\t\t";
                // line 85
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["players"]) ? $context["players"] : null), "bench", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
                    // line 86
                    echo "\t\t\t\t\t\t\t\t";
                    if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym", array(), "any", true, true) && (twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym")) > 0))) {
                        // line 87
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["playerName"] = $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym");
                        // line 88
                        echo "\t\t\t\t\t\t\t\t";
                    } else {
                        // line 89
                        echo "\t\t\t\t\t\t\t\t\t";
                        $context["playerName"] = ((twig_slice($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "firstname"), 0, 1) . ". ") . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "lastname"));
                        // line 90
                        echo "\t\t\t\t\t\t\t\t";
                    }
                    // line 91
                    echo "\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"), "html", null, true);
                    echo "\"";
                    if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id") == $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_in"), array(), "array"))) {
                        echo " selected";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, (isset($context["playerName"]) ? $context["playerName"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 93
                echo "\t\t\t\t\t\t</select>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<select name=\"sub";
                // line 95
                echo twig_escape_filter($this->env, (isset($context["subsNo"]) ? $context["subsNo"] : null), "html", null, true);
                echo "_position\" class=\"span2\"";
                if ((isset($context["subIsDisabled"]) ? $context["subIsDisabled"] : null)) {
                    echo " disabled";
                }
                echo ">
\t\t\t\t\t\t\t\t<option value=\"\">";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_substitutions_position_default"), "method"), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t";
                // line 97
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(array(0 => "T", 1 => "LV", 2 => "IV", 3 => "RV", 4 => "LM", 5 => "DM", 6 => "ZM", 7 => "OM", 8 => "RM", 9 => "LS", 10 => "MS", 11 => "RS"));
                foreach ($context['_seq'] as $context["_key"] => $context["subPosition"]) {
                    // line 98
                    echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_escape_filter($this->env, (isset($context["subPosition"]) ? $context["subPosition"] : null), "html", null, true);
                    echo "\"";
                    if (((isset($context["subPosition"]) ? $context["subPosition"] : null) == $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_position"), array(), "array"))) {
                        echo " selected";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("option_" . (isset($context["subPosition"]) ? $context["subPosition"] : null))), "method"), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subPosition'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 100
                echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<input type=\"number\" class=\"input-mini\" name=\"sub";
                // line 103
                echo twig_escape_filter($this->env, (isset($context["subsNo"]) ? $context["subsNo"] : null), "html", null, true);
                echo "_minute\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_minute"), array(), "array"), "html", null, true);
                echo "\"";
                if ((isset($context["subIsDisabled"]) ? $context["subIsDisabled"] : null)) {
                    echo " disabled";
                }
                echo " />
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<select name=\"sub";
                // line 106
                echo twig_escape_filter($this->env, (isset($context["subsNo"]) ? $context["subsNo"] : null), "html", null, true);
                echo "_condition\" class=\"span2\"";
                if ((isset($context["subIsDisabled"]) ? $context["subIsDisabled"] : null)) {
                    echo " disabled";
                }
                echo ">
\t\t\t\t\t\t\t";
                // line 107
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable(array(0 => "none", 1 => "Tie", 2 => "Leading", 3 => "Deficit"));
                foreach ($context['_seq'] as $context["_key"] => $context["condition"]) {
                    // line 108
                    echo "\t\t\t\t\t\t\t\t<option value=\"";
                    echo twig_escape_filter($this->env, (isset($context["condition"]) ? $context["condition"] : null), "html", null, true);
                    echo "\"";
                    if (($this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), (("sub" . (isset($context["subsNo"]) ? $context["subsNo"] : null)) . "_condition"), array(), "array") == (isset($context["condition"]) ? $context["condition"] : null))) {
                        echo " selected";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => ("formation_substitutions_condition_" . twig_lower_filter($this->env, (isset($context["condition"]) ? $context["condition"] : null)))), "method"), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['condition'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 110
                echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subsNo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "\t\t</tbody>
\t</table>
\t";
        }
        // line 117
        echo "\t
\t<h4>";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_freekickplayer"), "method"), "html", null, true);
        echo "</h4>
\t
\t<select name=\"freekickplayer\" id=\"freekickplayer\" class=\"span4\">
\t\t<option value=\"\"></option>
\t\t";
        // line 122
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["players"]) ? $context["players"] : null), "field", array(), "array"));
        foreach ($context['_seq'] as $context["_key"] => $context["player"]) {
            // line 123
            echo "\t\t\t";
            if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym", array(), "any", true, true) && (twig_length_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym")) > 0))) {
                // line 124
                echo "\t\t\t\t";
                $context["playerName"] = $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "pseudonym");
                // line 125
                echo "\t\t\t";
            } else {
                // line 126
                echo "\t\t\t\t";
                $context["playerName"] = ((twig_slice($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "firstname"), 0, 1) . ". ") . $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "lastname"));
                // line 127
                echo "\t\t\t";
            }
            // line 128
            echo "\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id"), "html", null, true);
            echo "\"";
            if (($this->getAttribute((isset($context["player"]) ? $context["player"] : null), "id") == $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), "freekickplayer"))) {
                echo " selected";
            }
            echo ">";
            echo twig_escape_filter($this->env, (isset($context["playerName"]) ? $context["playerName"] : null), "html", null, true);
            echo "</option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['player'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "\t</select>
\t
\t<h4>";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_offensive_title"), "method"), "html", null, true);
        echo "</h4>
\t
\t<p>";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "match_details_changes_max_help", 1 => $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getConfig", array(0 => "sim_allow_offensivechanges"), "method")), "method"), "html", null, true);
        echo "</p>
\t
\t";
        // line 136
        echo $context["formelements"]->getslider("offensive", $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), "offensive"));
        echo "
\t
\t\t<div class=\"form-horizontal\" style=\"margin-top: 10px\">
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label\" for=\"longpasses\">";
        // line 140
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_option_longpasses"), "method"), "html", null, true);
        echo " <i class=\"icon-question-sign wstooltip\" data-toggle=\"tooltip\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_option_longpasses_help"), "method"), "html", null, true);
        echo "\"></i></label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"make-switch\" 
\t\t\t\t\t\tdata-on-label=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_switch_on"), "method"), "html", null, true);
        echo "\" data-off-label=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_switch_off"), "method"), "html", null, true);
        echo "\" 
\t\t\t\t\t\tdata-on=\"success\" data-off=\"warning\">
\t\t\t\t\t\t<input type=\"checkbox\" id=\"longpasses\" name=\"longpasses\" value=\"1\"";
        // line 145
        if (($this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), "longpasses", array(), "any", true, true) && $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), "longpasses"))) {
            echo " checked";
        }
        echo ">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"control-group\">
\t\t\t\t<label class=\"control-label\" for=\"counterattacks\">";
        // line 151
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_option_counterattacks"), "method"), "html", null, true);
        echo " <i class=\"icon-question-sign wstooltip\" data-toggle=\"tooltip\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_option_counterattacks_help"), "method"), "html", null, true);
        echo "\"></i></label>
\t\t\t\t<div class=\"controls\">
\t\t\t\t\t<div class=\"make-switch\" 
\t\t\t\t\t\tdata-on-label=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_switch_on"), "method"), "html", null, true);
        echo "\" data-off-label=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "formation_switch_off"), "method"), "html", null, true);
        echo "\" 
\t\t\t\t\t\tdata-on=\"success\" data-off=\"warning\">
\t\t\t\t\t\t<input type=\"checkbox\" id=\"counterattacks\" name=\"counterattacks\" value=\"1\"";
        // line 156
        if (($this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), "counterattacks", array(), "any", true, true) && $this->getAttribute((isset($context["formation"]) ? $context["formation"] : null), "counterattacks"))) {
            echo " checked";
        }
        echo ">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t
\t\t<div class=\"form-actions\" style=\"text-align: center\">
\t\t\t<button type=\"submit\" class=\"btn btn-primary\">";
        // line 163
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_save"), "method"), "html", null, true);
        echo "</button>
\t\t\t<a href=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getInternalUrl", array(0 => "match", 1 => ("id=" . $this->getAttribute((isset($context["env"]) ? $context["env"] : null), "getRequestParameter", array(0 => "id"), "method"))), "method"), "html", null, true);
        echo "\" class=\"btn\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["i18n"]) ? $context["i18n"] : null), "getMessage", array(0 => "button_cancel"), "method"), "html", null, true);
        echo "</a>
\t\t</div>
\t\t<input type=\"hidden\" name=\"page\" value=\"match-live-changes\"/>
\t\t<input type=\"hidden\" name=\"action\" value=\"save-live-formation\"/>
</form>
";
    }

    public function getTemplateName()
    {
        return "views/match_live_changes.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  548 => 164,  544 => 163,  532 => 156,  525 => 154,  517 => 151,  506 => 145,  499 => 143,  491 => 140,  484 => 136,  479 => 134,  474 => 132,  470 => 130,  455 => 128,  452 => 127,  449 => 126,  446 => 125,  443 => 124,  440 => 123,  436 => 122,  429 => 118,  426 => 117,  421 => 114,  412 => 110,  397 => 108,  393 => 107,  385 => 106,  373 => 103,  368 => 100,  353 => 98,  349 => 97,  345 => 96,  337 => 95,  333 => 93,  318 => 91,  315 => 90,  312 => 89,  309 => 88,  306 => 87,  303 => 86,  299 => 85,  290 => 83,  285 => 80,  270 => 78,  267 => 77,  264 => 76,  261 => 75,  258 => 74,  255 => 73,  251 => 72,  242 => 70,  238 => 68,  235 => 67,  232 => 66,  229 => 65,  226 => 64,  223 => 63,  219 => 62,  212 => 58,  208 => 57,  204 => 56,  200 => 55,  191 => 50,  189 => 49,  184 => 46,  168 => 43,  164 => 41,  149 => 39,  145 => 38,  139 => 36,  136 => 35,  131 => 33,  126 => 32,  124 => 31,  118 => 30,  107 => 28,  104 => 27,  101 => 26,  98 => 25,  95 => 24,  92 => 23,  89 => 22,  72 => 21,  65 => 17,  61 => 16,  49 => 11,  46 => 10,  43 => 9,  36 => 6,  33 => 5,  28 => 3,  26 => 2,);
    }
}

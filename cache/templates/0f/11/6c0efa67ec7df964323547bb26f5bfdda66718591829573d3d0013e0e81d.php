<?php

/* blocks/messagesblock.twig */
class __TwigTemplate_0f116c0efa67ec7df964323547bb26f5bfdda66718591829573d3d0013e0e81d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["msgbox"] = $this->env->loadTemplate("macros/messagebox.twig");
        // line 2
        echo "

";
        // line 4
        if (array_key_exists("frontMessages", $context)) {
            // line 5
            echo "\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["frontMessages"]) ? $context["frontMessages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 6
                echo "\t\t";
                echo $context["msgbox"]->getbox($this->getAttribute((isset($context["message"]) ? $context["message"] : null), "title"), $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "message"), $this->getAttribute((isset($context["message"]) ? $context["message"] : null), "type"));
                echo "
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "blocks/messagesblock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 5,  25 => 4,  21 => 2,  19 => 1,  63 => 12,  59 => 11,  44 => 9,  41 => 8,  38 => 7,  32 => 6,  29 => 3,);
    }
}
